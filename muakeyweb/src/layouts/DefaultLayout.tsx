import React, { Suspense, useEffect } from 'react';
import { Layout, Spin } from 'antd';
const PermissionContent = React.lazy(() => import('../middleware/PermissionContent'));

const { Content } = Layout;

const loading = () => <Spin />;

const DefaultLayout = () => {

  return (
    <Layout className="site-layout" style={{ minHeight: '100vh' }}>
      <Layout>
        <Content style={{ overflow: 'initial' }}>
          <Suspense fallback={loading()}>
            <PermissionContent />
          </Suspense>
        </Content>
      </Layout>
    </Layout>
  );
};

export default DefaultLayout;
