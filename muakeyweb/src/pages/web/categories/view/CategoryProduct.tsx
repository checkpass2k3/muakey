import "../styles/category.scss"
import CategoryHead from "../components/CategoryHead"
import CategoryListBtn from "../components/CategoryListBtn"
import CategoryProductContent from "../components/CategoryProductContent"

const CategoryProduct = () => {
  return (
    <div className="header-container">
        <div className="category">
            <CategoryHead />
            <CategoryListBtn />
            <CategoryProductContent />
        </div>
    </div>
  )
}

export default CategoryProduct