import "../styles/category.scss"
import CategoryHead from "../components/CategoryHead";
import CategoryListBtn from "../components/CategoryListBtn";
import CategoryContent from "../components/CategoryContent";

const Category = () => {
  return (
    <div className='header-container'>
        <div className="category">
            <CategoryHead />
            <CategoryListBtn />
            <CategoryContent />
        </div>
    </div>
  )
}

export default Category

