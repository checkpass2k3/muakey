import { Input, Space,Row,Col } from 'antd';
import WrapperListProduct from './WrapperListProduct';
import "../styles/categorycontent.scss"
import {SearchOutlined} from "@ant-design/icons"
import SearchInput from './SearchInput';

const CategoryContent = () => {
    
    const { Search } = Input;
    
    return (
    <Row className="category-content">
        <Col md={0} xs={0} lg={6} className="category-content-search">
            <SearchInput />
            
        </Col>
        <Col md={24} xs={24} lg={18}>
            <WrapperListProduct />
        </Col>
    </Row>
  )
}

export default CategoryContent