import "../styles/categoryproductlist.scss"
import { Button } from "antd"
import {PlusOutlined} from "@ant-design/icons"
import positionPremit from "../../../../assets/images/position.jpg"

const CategoryProductList = () => {
  return (
    <div className="category-product">
        <div className="category-product__item">
            <div className="add-cart">
                <div className="add-cart__hover">
                    <img src={positionPremit} alt="" />
                    <div className="btn-add-cart">
                        <Button>
                            <PlusOutlined />
                            <span>Thêm giỏ hàng</span> 
                        </Button>
                    </div>            
                    <div className="sold-quantity">
                        <span className="sold">
                        Đã bán 966</span>
                    </div>
                </div>
                    
                <div className="price-title">
                    <a href=""><p className="title">Gói twitch Pubg Gói twitch Pubg Gói twitch Pubg Gói twitch Pubg Gói twitch Pubg</p></a>
                   <div className="quantity-sold">
                        <span className="sold">Đã bán 966</span>
                    </div> 
                    <p className="price">20.000đ</p>
                </div>
            </div>
        </div>
        <div className="category-product__item">
            <div className="add-cart">
                <div className="add-cart__hover">
                    <img src={positionPremit} alt="" />
                    <div className="btn-add-cart">
                        <Button>
                            <PlusOutlined />
                            <span>Thêm giỏ hàng</span> 
                        </Button>
                    </div>            
                    <div className="sold-quantity">
                        <span className="sold">
                        Đã bán 966</span>
                    </div>
                </div>
                    
                <div className="price-title">
                    <a href=""><p className="title">Gói twitch Pubg Gói twitch Pubg Gói twitch Pubg Gói twitch Pubg Gói twitch Pubg</p></a>
                   <div className="quantity-sold">
                        <span className="sold">Đã bán 966</span>
                    </div> 
                    <p className="price">20.000đ</p>
                </div>
            </div>
        </div>
        <div className="category-product__item">
            <div className="add-cart">
                <div className="add-cart__hover">
                    <img src={positionPremit} alt="" />
                    <div className="btn-add-cart">
                        <Button>
                            <PlusOutlined />
                            <span>Thêm giỏ hàng</span> 
                        </Button>
                    </div>            
                    <div className="sold-quantity">
                        <span className="sold">
                        Đã bán 966</span>
                    </div>
                </div>
                    
                <div className="price-title">
                    <a href=""><p className="title">Gói twitch Pubg Gói twitch Pubg Gói twitch Pubg Gói twitch Pubg Gói twitch Pubg</p></a>
                   <div className="quantity-sold">
                        <span className="sold">Đã bán 966</span>
                    </div> 
                    <p className="price">20.000đ</p>
                </div>
            </div>
        </div>
        <div className="category-product__item">
            <div className="add-cart">
                <div className="add-cart__hover">
                    <img src={positionPremit} alt="" />
                    <div className="btn-add-cart">
                        <Button>
                            <PlusOutlined />
                            <span>Thêm giỏ hàng</span> 
                        </Button>
                    </div>            
                    <div className="sold-quantity">
                        <span className="sold">
                        Đã bán 966</span>
                    </div>
                </div>
                    
                <div className="price-title">
                    <a href=""><p className="title">Gói twitch Pubg Gói twitch Pubg Gói twitch Pubg Gói twitch Pubg Gói twitch Pubg</p></a>
                   <div className="quantity-sold">
                        <span className="sold">Đã bán 966</span>
                    </div> 
                    <p className="price">20.000đ</p>
                </div>
            </div>
        </div>
        <div className="category-product__item">
            <div className="add-cart">
                <div className="add-cart__hover">
                    <img src={positionPremit} alt="" />
                    <div className="btn-add-cart">
                        <Button>
                            <PlusOutlined />
                            <span>Thêm giỏ hàng</span> 
                        </Button>
                    </div>            
                    <div className="sold-quantity">
                        <span className="sold">
                        Đã bán 966</span>
                    </div>
                </div>
                    
                <div className="price-title">
                    <a href=""><p className="title">Gói twitch Pubg Gói twitch Pubg Gói twitch Pubg Gói twitch Pubg Gói twitch Pubg</p></a>
                   <div className="quantity-sold">
                        <span className="sold">Đã bán 966</span>
                    </div> 
                    <p className="price">20.000đ</p>
                </div>
            </div>
        </div>
        <div className="category-product__item">
            <div className="add-cart">
                <div className="add-cart__hover">
                    <img src={positionPremit} alt="" />
                    <div className="btn-add-cart">
                        <Button>
                            <PlusOutlined />
                            <span>Thêm giỏ hàng</span> 
                        </Button>
                    </div>            
                    <div className="sold-quantity">
                        <span className="sold">
                        Đã bán 966</span>
                    </div>
                </div>
                    
                <div className="price-title">
                    <a href=""><p className="title">Gói twitch Pubg Gói twitch Pubg Gói twitch Pubg Gói twitch Pubg Gói twitch Pubg</p></a>
                   <div className="quantity-sold">
                        <span className="sold">Đã bán 966</span>
                    </div> 
                    <p className="price">20.000đ</p>
                </div>
            </div>
        </div>
        <div className="category-product__item">
            <div className="add-cart">
                <div className="add-cart__hover">
                    <img src={positionPremit} alt="" />
                    <div className="btn-add-cart">
                        <Button>
                            <PlusOutlined />
                            <span>Thêm giỏ hàng</span> 
                        </Button>
                    </div>            
                    <div className="sold-quantity">
                        <span className="sold">
                        Đã bán 966</span>
                    </div>
                </div>
                    
                <div className="price-title">
                    <a href=""><p className="title">Gói twitch Pubg Gói twitch Pubg Gói twitch Pubg Gói twitch Pubg Gói twitch Pubg</p></a>
                   <div className="quantity-sold">
                        <span className="sold">Đã bán 966</span>
                    </div> 
                    <p className="price">20.000đ</p>
                </div>
            </div>
        </div>
        <div className="category-product__item">
            <div className="add-cart">
                <div className="add-cart__hover">
                    <img src={positionPremit} alt="" />
                    <div className="btn-add-cart">
                        <Button>
                            <PlusOutlined />
                            <span>Thêm giỏ hàng</span> 
                        </Button>
                    </div>            
                    <div className="sold-quantity">
                        <span className="sold">
                        Đã bán 966</span>
                    </div>
                </div>
                    
                <div className="price-title">
                    <a href=""><p className="title">Gói twitch Pubg Gói twitch Pubg Gói twitch Pubg Gói twitch Pubg Gói twitch Pubg</p></a>
                   <div className="quantity-sold">
                        <span className="sold">Đã bán 966</span>
                    </div> 
                    <p className="price">20.000đ</p>
                </div>
            </div>
        </div>
        <div className="category-product__item">
            <div className="add-cart">
                <div className="add-cart__hover">
                    <img src={positionPremit} alt="" />
                    <div className="btn-add-cart">
                        <Button>
                            <PlusOutlined />
                            <span>Thêm giỏ hàng</span> 
                        </Button>
                    </div>            
                    <div className="sold-quantity">
                        <span className="sold">
                        Đã bán 966</span>
                    </div>
                </div>
                    
                <div className="price-title">
                    <a href=""><p className="title">Gói twitch Pubg Gói twitch Pubg Gói twitch Pubg Gói twitch Pubg Gói twitch Pubg</p></a>
                   <div className="quantity-sold">
                        <span className="sold">Đã bán 966</span>
                    </div> 
                    <p className="price">20.000đ</p>
                </div>
            </div>
        </div>
        <div className="category-product__item">
            <div className="add-cart">
                <div className="add-cart__hover">
                    <img src={positionPremit} alt="" />
                    <div className="btn-add-cart">
                        <Button>
                            <PlusOutlined />
                            <span>Thêm giỏ hàng</span> 
                        </Button>
                    </div>            
                    <div className="sold-quantity">
                        <span className="sold">
                        Đã bán 966</span>
                    </div>
                </div>
                    
                <div className="price-title">
                    <a href=""><p className="title">Gói twitch Pubg Gói twitch Pubg Gói twitch Pubg Gói twitch Pubg Gói twitch Pubg</p></a>
                   <div className="quantity-sold">
                        <span className="sold">Đã bán 966</span>
                    </div> 
                    <p className="price">20.000đ</p>
                </div>
            </div>
        </div>
        <div className="category-product__item">
            <div className="add-cart">
                <div className="add-cart__hover">
                    <img src={positionPremit} alt="" />
                    <div className="btn-add-cart">
                        <Button>
                            <PlusOutlined />
                            <span>Thêm giỏ hàng</span> 
                        </Button>
                    </div>            
                    <div className="sold-quantity">
                        <span className="sold">
                        Đã bán 966</span>
                    </div>
                </div>
                    
                <div className="price-title">
                    <a href=""><p className="title">Gói twitch Pubg Gói twitch Pubg Gói twitch Pubg Gói twitch Pubg Gói twitch Pubg</p></a>
                   <div className="quantity-sold">
                        <span className="sold">Đã bán 966</span>
                    </div> 
                    <p className="price">20.000đ</p>
                </div>
            </div>
        </div>
        <div className="category-product__item">
            <div className="add-cart">
                <div className="add-cart__hover">
                    <img src={positionPremit} alt="" />
                    <div className="btn-add-cart">
                        <Button>
                            <PlusOutlined />
                            <span>Thêm giỏ hàng</span> 
                        </Button>
                    </div>            
                    <div className="sold-quantity">
                        <span className="sold">
                        Đã bán 966</span>
                    </div>
                </div>
                    
                <div className="price-title">
                    <a href=""><p className="title">Gói twitch Pubg Gói twitch Pubg Gói twitch Pubg Gói twitch Pubg Gói twitch Pubg</p></a>
                   <div className="quantity-sold">
                        <span className="sold">Đã bán 966</span>
                    </div> 
                    <p className="price">20.000đ</p>
                </div>
            </div>
        </div>
    </div>
  )
}

export default CategoryProductList