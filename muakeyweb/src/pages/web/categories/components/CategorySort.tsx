import "../styles/categorysort.scss"
import { Tabs, Select } from "antd"
import CategoryProductList from "./CategoryProductList"

const CategorySort = () => {

  const { Option } = Select;

  return (
    <div className="category-sort">
        <div className="product-tab-container">
          <div className="product-tab-nav tab-item">
            <p className="sort-by">Sắp xếp theo</p>
            <Tabs type='card'>
              
              <Tabs.TabPane tab="Phổ biến" key="item-1">
                <CategoryProductList />
              </Tabs.TabPane>
              <Tabs.TabPane tab="Mới nhất" key="item-2">
                <CategoryProductList />
              </Tabs.TabPane>
              <Tabs.TabPane tab="Bán chạy" key="item-3">
                <CategoryProductList />
              </Tabs.TabPane>
              
            </Tabs>
            <Select className="select-category" defaultValue="Giá">
              <Option value="1">Từ thấp đến cao</Option>
              <Option value="2">Từ cao đến thấp</Option>
            </Select>
            <p className="quantity-product">24 sản phẩm</p>
          </div>
      </div>
    </div>
  )
}

export default CategorySort