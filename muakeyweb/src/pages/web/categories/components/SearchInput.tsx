import {SearchOutlined} from "@ant-design/icons"
import "../styles/searchinput.scss"

const SearchInput = () => {
  return (
    <div className='category-content-search-input'>
        <input type="text" placeholder='Tên sản phẩm / mã đơn hàng...' />
        <SearchOutlined className="icon-search" />
    </div>
  )
}

export default SearchInput