import { Button } from 'antd';
import "../styles/categorylistbtn.scss"

const CategoryListBtn = () => {
  return (
    <div className="category-list-btn">
        <Button>Nạp game mobile</Button>
        <Button>Game máy tính</Button>
        <Button>Gift Card</Button>
        <Button>Tài khoản tiện ích</Button>
        <Button>Xbox</Button>
        <Button>Prime Gaming</Button>
        <Button className='category-list-btn__active'>Tài khoản game giá rẻ</Button>
    </div>
  )
}

export default CategoryListBtn