import { RightOutlined,FilterOutlined } from "@ant-design/icons"
import '../styles/categoryhead.scss'

const CategoryHead = () => {

  return (
    <div className="category-head">

        <div className="category-head-link">
            <a href="">Trang chủ</a><RightOutlined />
            <a className="cate-active" href="">Danh sách danh mục</a>
        </div>
        <div className="category-head-title">
          <p>Danh sách danh mục</p>
          <div className="category-head-title__filter">
            <FilterOutlined />
            <span className="text-filter">Lọc</span>
            </div>
        </div>
        <p className="sub-heading">Check out our current deals, they are here for a limited time only!</p>
    </div>
  )
}

export default CategoryHead