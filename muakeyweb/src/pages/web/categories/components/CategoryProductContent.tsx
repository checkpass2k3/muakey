import "../styles/categoryproductcontent.scss"
import "../styles/categorycontent.scss"
import "../styles/pagination.scss"
import CategorySort from './CategorySort';
import { Space,Row,Col,Pagination,InputNumber,Checkbox, Button } from 'antd';
import { UpOutlined,DownOutlined,SearchOutlined } from "@ant-design/icons";
import CategoryProductList from "./CategoryProductList";
import SearchInput from "./SearchInput";

const CategoryProductContent = () => {
  
  return (
    <Row className="category-content">
        <Col md={0} xs={0} lg={6} className="category-content-search">
            <Space className="vertical" direction="vertical">
                <SearchInput />
                
                <div className="price-range">
                  <p>Khoảng giá</p>
                  <div className="input-number">
                    <InputNumber min={0} max={10} defaultValue={0} />
                    <span className="minus">-</span>
                    <InputNumber min={0} max={10} defaultValue={0} />
                  </div>
                </div>
                <div className='category-filter'>
                  <div className="category-filter-item">
                    <p>Khu vực kích hoạt</p>
                    <ul className="auto-scroll">
                      <li><Checkbox>Checkbox</Checkbox></li>
                      <li><Checkbox>Checkbox</Checkbox></li>
                      <li><Checkbox>Checkbox</Checkbox></li>
                      <li><Checkbox>Checkbox</Checkbox></li>
                      <li><Checkbox>Checkbox</Checkbox></li>
                      <li><Checkbox>Checkbox</Checkbox></li>
                    </ul>
                    {/* <p className="view-more">Xem thêm <UpOutlined /></p> */}
                    <p className="view-more">Thu gọn <DownOutlined /></p>
                  </div>
                  <div className="category-filter-item">
                    <p>Khu vực kích hoạt</p>
                    <ul className="auto-scroll">
                      <li><Checkbox>Checkbox</Checkbox></li>
                      <li><Checkbox>Checkbox</Checkbox></li>
                      <li><Checkbox>Checkbox</Checkbox></li>
                      <li><Checkbox>Checkbox</Checkbox></li>
                      <li><Checkbox>Checkbox</Checkbox></li>
                      <li><Checkbox>Checkbox</Checkbox></li>
                    </ul>
                    {/* <p className="view-more">Xem thêm <UpOutlined /></p> */}
                    <p className="view-more">Thu gọn <DownOutlined /></p>
                  </div>
                  <div className="category-filter-item">
                    <p>Khu vực kích hoạt</p>
                    <ul className="auto-scroll">
                      <li><Checkbox>Checkbox</Checkbox></li>
                      <li><Checkbox>Checkbox</Checkbox></li>
                      <li><Checkbox>Checkbox</Checkbox></li>
                      <li><Checkbox>Checkbox</Checkbox></li>
                      <li><Checkbox>Checkbox</Checkbox></li>
                      <li><Checkbox>Checkbox</Checkbox></li>
                    </ul>
                    {/* <p className="view-more">Xem thêm <UpOutlined /></p> */}
                    <p className="view-more">Thu gọn <DownOutlined /></p>
                  </div>
                  <div className="category-filter-item">
                    <p>Khu vực kích hoạt</p>
                    <ul className="auto-scroll">
                      <li><Checkbox>Checkbox</Checkbox></li>
                      <li><Checkbox>Checkbox</Checkbox></li>
                      <li><Checkbox>Checkbox</Checkbox></li>
                      <li><Checkbox>Checkbox</Checkbox></li>
                      <li><Checkbox>Checkbox</Checkbox></li>
                      <li><Checkbox>Checkbox</Checkbox></li>
                    </ul>
                    {/* <p className="view-more">Xem thêm <UpOutlined /></p> */}
                    <p className="view-more">Thu gọn <DownOutlined /></p>
                  </div>
                  
                </div>
                <Button className="remove-filter">Xóa lọc</Button>
            </Space>
        </Col>
        <Col md={24} xs={24} lg={18}>
             <CategorySort /> {/* CÓ dùng thư viên */}
             {/* Không dùng thư viên <div className="btn-cate-product">
              <p>Sắp xếp theo</p>
              <div className="btn-cate-product__list">
                <div className="btn-cate-product__list--item cate-pro-active">Phổ biến</div>
                <div className="btn-cate-product__list--item">Mới nhất</div>
                <div className="btn-cate-product__list--item">Bán chạy</div>
                <select name="" id="">
                  <option value="">Giá</option>
                  <option value="">2</option>
                  <option value="">3</option>
                </select>
              </div>
              
              <p>24 sản phẩm</p>
             </div>
            <CategoryProductList /> */}
            <Pagination className="ml-0" defaultCurrent={1} total={50} />
        </Col>
    </Row>
  )
}

export default CategoryProductContent