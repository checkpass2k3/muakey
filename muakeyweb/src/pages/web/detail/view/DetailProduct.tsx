import "../styles/detailpro.scss"
import { RightOutlined,BellFilled,HeartFilled,MinusOutlined,PlusOutlined,AlertOutlined,StarFilled } from "@ant-design/icons"
import { Button} from "antd"
import arknights from "../../../../assets/images/arknights.jpg"
import rating from "../../../../assets/images/ratting.svg"
import area from "../../../../assets/images/area.svg"
import questionMark from "../../../../assets/images/question-mark.svg"
import onMobile from "../../../../assets/images/on-mobile.svg"
import app from "../../../../assets/images/app.svg"
import {useState} from "react"
import '../../categories/styles/pagination.scss'
import SaleSlider from "../../home/components/SaleSlider"
import Comment from "../components/Comment"

const DetailProduct = () => {

	const [quantity,setQuantity] = useState<number>(1)

  return (
    <div className="detail-product">
        <div className="head-link">
            <a href="">Trang chủ</a><RightOutlined />
            <a href="">Tất cả danh mục</a><RightOutlined />
            <a href="">EBAY GIFT CARD (US)</a><RightOutlined />
            <a className="detail-active" href="">EBAY USD10 GIFT CARD (US)</a>
        </div>
		
        <div className="product">
          <div className="product-row">
            <div className="product-left">
              <div className="product-image">
				<img src={arknights} alt="" />
			  </div>
            </div>
			<div className="product-right flex-1">
				<div className="product-name">40 Kim Cương - 30$ Arknights</div>
				<div className="product-review display-flex">
					<div className="product-review__star">
						<StarFilled />
						<StarFilled />
						<StarFilled />
						<StarFilled />
						<StarFilled />
					</div>
					<div className="product-review__count">từ 0 đánh giá</div>
					<div className="product-review__sold">Đã bán <span>51</span></div>
				</div>
				<div className="product-order display-flex">
					<div className="btn-order">
						<Button>Order</Button>
					</div>
					<div className="product-bell center">
						<BellFilled style={{color: "yellow",fontSize: "20px",margin:"0 10px"}} />
						<span>Nhấn chuông để nhận thông báo giảm giá</span>
					</div>
					<div className="product-favorite favorite-active center">
						<HeartFilled className="icon-heart" />
						<span className="text-favorite">Yêu thích</span>
					</div>
				</div>
				<div className="product-area display-flex">
					<div className="product-area-active display-flex flex-1">
						<div className="product-area-active__image">
							<img src={area} alt="" />
						</div>
						<div className="product-area-activated">
							<p className="fw-600">Khu vực kích hoạt</p>
							<p className="can-active fz-12">Có thể kích hoạt tại:</p>
							<div className="area-activated-at display-flex">
								<div className="area-activated-at__image">
									<img src={area} alt="" />
								</div>
								<span>GLOBAL</span>
							</div>
						</div>
					</div>
					<div className="product-brand display-flex flex-1">
						<div className="product-area-active__image">
							<img src={questionMark} alt="" />
						</div>
						<div className="product-brand-active">
							<p className="fw-600">Thương hiệu</p>
							<p><span className="text-12">Có thể kích hoạt trên</span><span>TH KHÁC</span></p>
							<div className="area-activated-at">
							<a href="" className="text-12"><span>Hướng dẫn kích hoạt</span></a>	
							</div>
						</div>
					</div>
					<div className="product-work-mobile display-flex flex-1">
						<div className="product-area-active__image">
							<img src={onMobile} alt="" />
						</div>
						<div>
							<p className="fw-600">WORK ON</p>
							<p className="fz-12">MOBILE</p>
						</div>
					</div>
					<div className="product-work-mobile display-flex flex-1">
						<div className="product-area-active__image">
							<img src={app} alt="" />
						</div>
						<div>
							<p className="fw-600">Loại sản phẩm</p>
							<p className="fz-12">IN-APP</p>
						</div>
					</div>
				</div>
			</div>
          </div>
		  <div className="alert">
			<div className="display-flex alert-content">
				<AlertOutlined className="center" />
				<div>
					<p>Thông báo quan trọng</p> 
					<div>Lưu ý 1 Tài khoản Riot chỉ sử dụng 1 lần trong thời gian từ 7/1 tới 28/1</div> 
				</div>
			</div>
		</div>
		  <div className="order-product">
			<div className="order-product__left">
				<div className="card card-active">
					<div className="card-image">
						<img src={arknights} alt="" />
					</div>
					<div className="card-center">
						<div className="card-center-name">40 Kim Cương - 30$ Arknights</div>
						<div className="btn-order">
							<Button>Order</Button>
						</div>
					</div>
					<div className="product-price">
						585.000đ
					</div>
				</div>
				<div className="card">
					<div className="card-image">
						<img src={arknights} alt="" />
					</div>
					<div className="card-center">
						<div className="card-center-name">40 Kim Cương - 30$ Arknights</div>
						<div className="btn-order">
							<Button style={{background: "rgb(220, 116, 57)"}}>pre-order</Button>
						</div>
					</div>
					<div className="product-price">
						585.000đ
					</div>
				</div>
				
			</div>
			<div className="order-product__right">
				<div className="order-product__right--quantity">
					<div className="label">
						Số lượng
					</div>
					<div className="quantity-control">
						<MinusOutlined onClick={() => setQuantity(quantity !== 1 ? quantity-1: quantity)} />
						<div className="quantity">{quantity}</div>
						<PlusOutlined onClick={() => setQuantity(quantity+1)} />
					</div>
				</div>
				<div className="order-product__right--money">
					<div className="cost display-flex between">
						<div>Giá gốc</div>
						<div>585.000đ</div>
					</div>
					<div className="discount display-flex between">
						<div>Giảm giá</div>
						<div>0đ (0%)</div>
					</div>
					<div className="line"></div>
					<div className="total-money display-flex between">
						<div>Tổng tiền</div>
						<div className="color-money">585.000đ</div>
					</div>
					<div className="display-flex between cart-now-buy">
						<Button>Giỏ hàng</Button>
						<Button className="now-buy">Mua ngay</Button>
					</div>
				</div>
			</div>
		  </div>
        </div> 	
		<Comment />
		<div className="des-detail">
			<div className="head" id="des-detail">Mô tả chi tiết</div>
			<p>Khách hàng vui lòng gửi thông tin đăng nhập là tài khoản + mật khẩu ních liên kết với game ví dụ facebook, Twitter, google, Gamecenter, Icloud,...</p>
			<p>Lưu ý: PUBG Mobile và TFT mobile Shop không hỗ trợ nạp bằng ních Google vì vậy anh em liên kết với Facebook hoặc các phương thức khác mà shop hỗ trợ</p>
			<p>Shop Game69.vn cam kết bảo mật thông tin tài khoản của anh em 100%.</p>
			<p>Nạp bảo hành trọn đời - không bao giờ nạp lậu.</p>
			<iframe width="560" height="200" src="https://www.youtube.com/embed/fGqRO5DwFjA" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
		</div>
		<div className="connexion">
			<div className="title-head">
				<div className="">
					Liên quan
				</div>
				<a href="">Xem tất cả</a>
			</div>
			<div className="connexion-content">
				<SaleSlider />
			</div>
		</div>
		<div className="connexion">
			<div className="title-head">
				<div className="">
					Được tìm kiếm nhiều
				</div>
				<a href="">Xem tất cả</a>
			</div>
			<div className="connexion-content">
				<SaleSlider />
			</div>
		</div>
    </div>
  )
}

export default DetailProduct