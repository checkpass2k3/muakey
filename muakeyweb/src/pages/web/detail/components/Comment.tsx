import "../styles/comment.scss"
import {StarFilled,FileFilled,SendOutlined,LikeOutlined,PlayCircleOutlined,ShoppingOutlined} from "@ant-design/icons"
import {Pagination,Avatar } from "antd"
import goldRank from "../../../../assets/images/gold-rank.svg"
import avatar from "../../../../assets/images/avatar.jpg"

const Comment = () => {
  return (
    <div className="comment-and-rate">
			<div className="comment-and-rate-head">
				<a href="#comment-and-rate" className="center">
					<StarFilled style={{fontSize: "20px", marginRight: "3px"}} />
					<p>Bình luận và đánh giá</p>
				</a >
				<a href="#des-detail" className="center">
					<FileFilled style={{fontSize: "20px", marginRight: "3px"}} />
					<p>Mô tả chi tiết</p>
				</a >
			</div>
			<div className="comment-and-rate-body">
				<div className="display-flex">
					<div className="input-comment-rate">
					<div className="heading" id="comment-and-rate">Bình luận & đánh giá</div>
					<div className="input-comment display-flex">
						<div className="user-comment center">
							<img src={avatar} alt="" />
						</div>
						
                        <input className="flex-1" type="text" placeholder="Bình luận..." />
                        <button><SendOutlined style={{fontSize: "16px"}} /></button>
					</div>
					
				</div>
					
				<div className="rate">
					<div className="count-comment">
						<div>
						<span>1</span>
						<p>Bình luận</p>
						</div>
						
					</div>
					<div className="count-star">
						<span>0.0 <StarFilled style={{color: "#ffca06"}} /></span>
						<p>Đánh giá</p>
					</div>
				</div>
				</div>
				<div className="comment">
					<div className="display-flex">
						<div className="user-comment">
							<img src={avatar} alt="" />
						</div>
						<div className="name-user">
							<div className="user">minato0107</div>
							<div className="display-flex">
								<img src={goldRank} alt="" />
								<div className="name">ROOKIE</div>
							</div>
							<div className="content-comment">Giá ổn</div>
							<div className="like-reply display-flex">
								<div className="like">
									<LikeOutlined style={{marginRight: "5px"}} />
									<span>0 thích</span>
								</div>
								<div className="reply">
								<PlayCircleOutlined style={{marginRight: "5px"}} />
									<span>0 trả lời</span>
								</div>
							</div>
						</div>
					</div>
					<div className="time-ago">
						<div className="">
							<span className="sold">Đã mua <ShoppingOutlined /></span> 
							<span className="star">
								<StarFilled style={{color: "yellow"}} />
								<StarFilled style={{color: "yellow"}} />
								<StarFilled style={{color: "yellow"}} />
								<StarFilled style={{color: "yellow"}} />
								<StarFilled style={{color: "yellow"}} />
							</span>
						</div>
						18 tháng 1, 2023 - 18:41
					</div>
				</div>
				<div className="reply-comment">
					<div className="display-flex between">
						<div className="display-flex">
							<div className="user-comment">
								<img src={avatar} alt="" />
							</div>
							<div className="name-user">
								<div className="user">minato0107</div>
								<div className="display-flex">
									<img src={goldRank} alt="" />
									<div className="name">ROOKIE</div>
								</div>
								<div className="content-comment">Giá ổn</div>
								<div className="like-reply display-flex">
									<div className="like">
										<LikeOutlined style={{marginRight: "5px"}} />
										<span>0 thích</span>
									</div>
								</div>
							</div>
						</div>

						<div className="time-ago">
							<div className="">
								<span className="sold">Đã mua <ShoppingOutlined /></span>
								<span className="star">
									<StarFilled style={{color: "yellow"}} />
									<StarFilled style={{color: "yellow"}} />
									<StarFilled style={{color: "yellow"}} />
									<StarFilled style={{color: "yellow"}} />
									<StarFilled style={{color: "yellow"}} />
								</span>
							</div>
							18 tháng 1, 2023 - 18:41
						</div>
					</div>
					<div className="input-reply-comment display-flex">
						<div className="user-comment"></div>
						<input className="flex-1" type="text" placeholder="Bình luận..." />
                        <button><SendOutlined style={{fontSize: "16px"}} /></button>
					</div>	
				</div>
				<Pagination className="ml-0 pagination-comment" defaultCurrent={1} total={50} />
			</div>
		</div>
  )
}

export default Comment