import { Button, Form, Input } from "antd";
import { FaEnvelope } from "react-icons/fa";
import '../styles/step2.scss';

const Step2 = ({setStep} : any) => {

    const handleClick = () => {
        setStep.setStep2(false);    
        setStep.setStep3(true);    
    }

    const handleBack = () => {
        setStep.setStep1(true);
        setStep.setStep2(false); 
    }

    return (
        <>
            <div className="font-size-24 font-weight-medium color-light">Chúng tôi đã gửi email xác nhận đến bạn</div>
            <div className="content-form">
                <Form
                    name="basic"
                    labelCol={{ span: 8 }}
                    wrapperCol={{ span: 16 }}
                    autoComplete="off"
                >
                    <Form.Item
                        className='form-email'
                        name="email"
                    >
                        <Input className='email-input border-rd-4 background-none' prefix={<FaEnvelope className='email-icon font-size-24' />} placeholder="YourEmail@gmail.com" />
                    </Form.Item>
                    <div className="form-desc font-size-12">
                        Click vào link ở email xác nhận để hoàn thành quá trình. Vui lòng
                        <span className="desc-note"> THƯ RÁC </span>
                        hoặc 
                        <span className="desc-note"> SPAM </span>
                        nếu bạn không tìm thấy ở
                        <span className="desc-note"> HỘP THƯ ĐẾN</span>
                        .
                    </div>
                    <Form.Item className='form-btn display-flex-center justify-content-center border-rd-8'>
                        <Button onClick={handleClick} className='btn-submit background-none border-none full-width display-flex-center justify-content-center' type="primary" htmlType="submit">
                            Kiểm tra mail
                        </Button>
                    </Form.Item>
                </Form>
                <Button onClick={handleBack} className='btn-back border-rd-8 border-none full-width display-flex-center justify-content-center'>
                    Quay lại            
                </Button>
            </div>
        </>
    )
}

export default Step2;