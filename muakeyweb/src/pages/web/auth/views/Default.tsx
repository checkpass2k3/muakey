import { Col, Row } from "antd";
import React, { useEffect, useState } from "react";
import { Route, Routes, useLocation } from "react-router-dom";
import '../styles/default.scss';
import '../styles/login.scss';

const Login = React.lazy(() => import('./Login'));
const Register = React.lazy(() => import('./Register'));

const Default = () => {
    const location = useLocation();
    const [isPageLogin, setIsPageLogin] = useState<boolean>(false);

    useEffect(() => {
        if (location.pathname === '/web/login') {
          setIsPageLogin(true);
        } else {
          setIsPageLogin(false);
        }
      }, [location])

    return (
        <div className="auth-wrapper overflow-hidden">
            <div className="container full-height">
                <div className="auth-container position-rel display-flex-center display-flex-column full-height">
                    <div className="auth-body position-rel full-width">
                        { isPageLogin ? <Login /> : <Register /> }
                    </div>
                    <div className="auth-footer bottom-0 full-width font-weight-medium display-flex-center display-flex-column flex-justify-between">
                        <div className="auth-list-link display-flex-wrap">
                            <a className='auth-link text-center' href="#">Điều khoản dịch vụ</a>
                            <a className='auth-link text-center' href="#">Chính sách bảo mật</a>
                            <a className='auth-link text-center' href="#">Hỗ trợ</a>
                        </div>
                        <div className="auth-copyright text-center">Copyright © 2021 Muakey. All Rights Reserved.</div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Default;