import {Col, Row } from "antd";
import React, { useState } from "react";
import '../styles/register.scss';

const Step1 = React.lazy(() => import('./Step1'));
const Step2 = React.lazy(() => import('./Step2'));
const Step3 = React.lazy(() => import('./Step3'));
const Step4 = React.lazy(() => import('./Step4'));

const Register = () => {
    const [step1, setStep1] = useState<boolean>(true);
    const [step2, setStep2] = useState<boolean>(false);
    const [step3, setStep3] = useState<boolean>(false);
    const [step4, setStep4] = useState<boolean>(false);
    const arrayStep = [step1, step2, step3];

    const numbers = document.querySelectorAll('.step-number');

    arrayStep.forEach((step, i) => {
        if (step) {
            numbers.forEach((number, j) => {
                number.classList.remove('step-active');
                if (i == j) {
                    number.classList.add('step-active');
                }
            })
        }
    })

    return (
        <Row className='register-body full-width margin-auto overflow-hidden border-rd-20'>
            <Col className='register-banner display-flex-center' xs={{ span: 24, order: 2}} sm={{ span: 24, order: 2}} md={{ span: 12, order: 1}}>
                <div className="banner-wrapper full-width">
                    <div className="banner-heading font-size-18 font-weight-medium ">4 bước để đăng ký trở thành thành viên Muakey.com</div>
                    <div className="banner-step">
                        <div className="step-item display-flex font-size-14 font-weight-normal">
                            <div className="step-number step-active display-flex-center justify-content-center border-rd-4">1</div>
                            <div className="step-content">Đăng ký bằng email để nhận được email xác minh.</div>
                        </div>
                        <div className="step-item display-flex font-size-14 font-weight-normal">
                            <div className="step-number display-flex-center justify-content-center border-rd-4">2</div>
                            <div className="step-content">Nhấp vào liên kết xác minh trong email đã đăng ký.</div>
                        </div>
                        <div className="step-item display-flex font-size-14 font-weight-normal">
                            <div className="step-number display-flex-center justify-content-center border-rd-4">3</div>
                            <div className="step-content">Hoàn thành biểu mẫu đăng ký.</div>
                        </div>
                        <div className="step-item display-flex font-size-14 font-weight-normal">
                            <div className="step-number display-flex-center justify-content-center border-rd-4">4</div>
                            <div className="step-content">Bạn đã sẵn sàng, hãy bắt đầu tận hưởng mua sắm trong Muakey.com</div>
                        </div>
                    </div>
                </div>
            </Col>
            <Col className='register-content display-flex-center' xs={{ span: 24, order: 1}} sm={{ span: 24, order: 1}} md={{ span: 12, order: 2}}>
                <div className="content-wrapper full-width font-size-14">
                { step1 ? <Step1 setStep={{ setStep1, setStep2 }} /> : null}
                { step2 ? <Step2 setStep={{ setStep1, setStep2, setStep3 }} /> : null}
                { step3 ? <Step3 setStep={{ setStep3, setStep4 }} /> : null}
                </div>
            </Col>
        </Row>
    )
}

export default Register;