import { Button, Form, Input } from 'antd';
import { FaDiscord, FaEnvelope, FaFacebook, FaGoogle, FaSteam, FaTiktok } from "react-icons/fa";
import '../styles/step1.scss';

const Step1 = ({setStep} : any) => {

    const handleClick = () => {
        setStep.setStep1(false);    
        setStep.setStep2(true);    
    }

    return (
        <>
            <div className="font-size-24 font-weight-medium color-light">Đăng ký</div>
            <div className="content-title">Đăng ký bằng email để nhận được email xác minh.</div>
            <div className="content-form">
                <Form
                    name="basic"
                    labelCol={{ span: 8 }}
                    wrapperCol={{ span: 16 }}
                    autoComplete="off"
                >
                    <Form.Item
                        className='form-email'
                        name="email"
                        rules={[{ required: true, message: 'Please input your email or phone number!' }]}
                    >
                        <Input className='email-input border-rd-4 background-none' prefix={<FaEnvelope className='email-icon font-size-24' />} placeholder="Email/Số điện thoại" />
                    </Form.Item>
                    <Form.Item className='form-btn display-flex-center justify-content-center border-rd-8'>
                        <Button onClick={handleClick} className='btn-submit background-none border-none full-width display-flex-center justify-content-center' type="primary" htmlType="submit">
                            Gửi thư xác minh
                        </Button>
                    </Form.Item>
                </Form>
            </div>
            <div className="content-social font-weight-normal">
                <div className="social-title">Đăng nhập với tài khoản mạng xã hội</div>
                <div className="social-list display-flex-center-wrap">
                    <a href='#' className="social-item display-flex-center justify-content-center font-size-18 border-rd-4"><FaFacebook className='social-icon' /></a>
                    <a href='#' className="social-item display-flex-center justify-content-center font-size-18 border-rd-4"><FaTiktok className='social-icon' /></a>
                    <a href='#' className="social-item display-flex-center justify-content-center font-size-18 border-rd-4"><FaDiscord className='social-icon' /></a>
                    <a href='#' className="social-item display-flex-center justify-content-center font-size-18 border-rd-4"><FaGoogle className='social-icon' /></a>
                    <a href='#' className="social-item display-flex-center justify-content-center font-size-18 border-rd-4"><FaSteam className='social-icon' /></a>
                </div>
            </div>
        </>
    )
}

export default Step1;