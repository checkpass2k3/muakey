import { Button, Col, Form, Input, Row } from 'antd';
import '../styles/login.scss';
import { FaArrowRight, FaDiscord, FaFacebook, FaGoogle, FaLock, FaSteam, FaTiktok, FaUserCircle } from "react-icons/fa";
import { Link } from 'react-router-dom';

const Login = () => {
    return (
        <Row className='login-body border-rd-20 overflow-hidden margin-auto full-width'>
            <Col className='login-banner display-flex-center' xs={{ span: 24, order: 2}} sm={{ span: 24, order: 2}} md={{ span: 12, order: 1}}>
                <div className="banner-wrapper full-width">
                    <a className='banner-logo display-block' href="https://www.youtube.com/"></a>
                    <div className='banner-heading font-size-18 font-weight-medium '>Muakey, Mua Là Rẻ!</div>
                    <div className='banner-title font-size-16 font-weight-normal'>Lần đầu tiên ở đây? Tạo tài khoản của bạn để mua sắm trên Muakey.com</div>
                    <Link to="/web/register" className='banner-btn display-inline-block'>
                        <Button className='btn-register border-rd-8 display-flex-center justify-content-center border-none font-size-14 font-weight-semibold' type="primary">Tạo tài khoản</Button>
                    </Link>
                </div>
            </Col>
            <Col className='login-content display-flex-center' xs={{ span: 24, order: 1}} sm={{ span: 24, order: 1}} md={{ span: 12, order: 2}}>
                <div className="content-wrapper full-width">
                    <div className="font-size-24 font-weight-medium">Đăng nhập</div>
                    <div className="content-form">
                        <Form
                            name="basic"
                            labelCol={{ span: 8 }}
                            wrapperCol={{ span: 16 }}
                            autoComplete="off"
                            >
                            <Form.Item
                                className='form-email'
                                name="email"
                                rules={[{ required: true, message: 'Please input your email!' }]}
                            >
                                <Input className='email-input border-rd-4 background-none' prefix={<FaUserCircle className='email-icon font-size-24' />} placeholder="Email" />
                            </Form.Item>

                            <Form.Item
                                className='form-password position-rel '
                                name="password"
                                rules={[{ required: true, message: 'Please input your password!' }]}
                            >
                                <Input.Password className='password-input padding-0 border-rd-4 background-none' prefix={<FaLock className='password-icon font-size-24' />} placeholder="Password"/>
                                <Form.Item className='form-btn border-rd-4 bottom-0 right-0 top-0 display-flex-center justify-content-center position-abs'>
                                    <Button className='btn-submit display-flex-center background-none border-none' type="primary" htmlType="submit">
                                        <FaArrowRight className='font-size-16 cursor-pointer' />
                                    </Button>
                                </Form.Item>
                            </Form.Item>
                        </Form>
                    </div>
                    <a href="#" className="forgot-password display-block font-size-16 font-weight-medium ">Quên mật khẩu?</a>
                    <div className="content-social font-weight-normal">
                        <div className="social-title">Đăng nhập với tài khoản mạng xã hội</div>
                        <div className="social-list display-flex-center-wrap">
                            <a href='#' className="social-item display-flex-center justify-content-center font-size-18 border-rd-4"><FaFacebook className='social-icon' /></a>
                            <a href='#' className="social-item display-flex-center justify-content-center font-size-18 border-rd-4"><FaTiktok className='social-icon' /></a>
                            <a href='#' className="social-item display-flex-center justify-content-center font-size-18 border-rd-4"><FaDiscord className='social-icon' /></a>
                            <a href='#' className="social-item display-flex-center justify-content-center font-size-18 border-rd-4"><FaGoogle className='social-icon' /></a>
                            <a href='#' className="social-item display-flex-center justify-content-center font-size-18 border-rd-4"><FaSteam className='social-icon' /></a>
                        </div>
                    </div>
                </div>
            </Col>
        </Row>
    )
}

export default Login;