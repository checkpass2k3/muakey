import { Button } from 'antd';
import '../styles/step4.scss';
import banner from '../../../../assets/images/register/register-success.png';
import { useNavigate } from 'react-router';

const Step4 = () => {
    const navigate = useNavigate();

    const handleClick = () => {
        navigate('/web');       
    }

    return (
        <div className="auth-wrapper-success overflow-hidden">
            <div className="container full-box">
                <div className="auth-container position-rel full-height">
                    <div className="auth-body flex-justify-center full-box">
                        <div className="register-success">
                            <div className="text-center">
                                <img src={banner} alt="banner"/>
                            </div>
                            <div className="success-heading font-size-24 text-center font-weight-medium">Cám ơn vì đã tham gia Muakey.com</div>
                            <div className="success-title font-size-16 text-center font-weight-medium">Bạn đã đăng ký thành công!</div>
                            <Button onClick={handleClick} className='btn-back display-flex-center justify-content-center font-size-14 border-rd-8 border-none margin-auto font-weight-semibold' type='primary'>Quay về trang chủ</Button>
                        </div>
                    </div>
                    <div className="auth-footer font-size-16 bottom-0 full-width font-weight-medium display-flex-center display-flex-column flex-justify-between">
                        <div className="auth-list-link display-flex-wrap">
                            <a className='auth-link text-center' href="#">Điều khoản dịch vụ</a>
                            <a className='auth-link text-center' href="#">Chính sách bảo mật</a>
                            <a className='auth-link text-center' href="#">Hỗ trợ</a>
                        </div>
                        <div className="auth-copyright text-center">Copyright © 2021 Muakey. All Rights Reserved.</div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Step4;