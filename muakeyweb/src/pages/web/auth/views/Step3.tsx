import { Button, Form, Input } from "antd";
import { FaCheckCircle, FaEnvelope } from "react-icons/fa";
import { useNavigate } from "react-router-dom";
import '../styles/step3.scss';

const Step3 = ({setStep} : any) => {
    
    const navigate = useNavigate();

    const handleClick = () => {
        setStep.setStep3(false); 
        navigate('/web/register-success'); 
    }

    return (
        <>
            <div className="font-size-24 font-weight-medium color-light">Chào mừng quay trở lại!</div>
            <div className="content-desc">Để trở thành thành viên của Muakey.com, vui lòng hoàn thành thông tin dưới đây:</div>
            <div className="content-form">
                <Form
                    name="basic"
                    labelCol={{ span: 8 }}
                    wrapperCol={{ span: 16 }}
                    autoComplete="off"
                >
                    <div className="form-item display-flex">
                        <div className="form-prefix display-flex-center ">Email</div>
                        <Form.Item
                            className='form-email'
                            name="email"
                        >
                            <Input className='email-input border-none border-rd-4 background-none' prefix={<FaEnvelope className='email-icon font-size-20' />} placeholder="YourEmail@gmail.com" />
                        </Form.Item>
                    </div>

                    <div className="form-item display-flex">
                        <div className="form-prefix display-flex-center ">Username</div>
                        <div className="full-width">
                            <Form.Item
                                className="form-primary full-width"
                                name="username"
                                rules={[{ required: true, message: 'Please input your Username!' }]}
                            >
                                <Input className="input-primary font-size-16 display-flex-center border-rd-4 background-none" placeholder="Username" />
                            </Form.Item>
                            <div className="rules display-flex">
                                <div className="rule-item display-flex-center font-size-12">
                                    <FaCheckCircle className='rule-icon font-size-14' />
                                    <div className="rule-message">Gồm 6 ~ 32 ký tự</div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="form-item display-flex">
                        <div className="form-prefix display-flex-center ">Mật khẩu</div>
                        <div className="full-width">
                            <Form.Item
                                className='form-primary full-width'
                                name="password"
                                rules={[{ required: true, message: 'Please input your password!' }]}
                            >
                                <Input.Password className='input-primary display-flex-center border-rd-4 background-none' placeholder="Mật khẩu"/>
                            </Form.Item>
                            <div className="rules display-flex-column ">
                                <div className="rule-item display-flex font-size-12">
                                    <FaCheckCircle className='rule-icon font-size-14' />
                                    <div className="rule-message">Gồm 6 ~ 20 ký tự</div>
                                </div>
                                <div className="rule-item display-flex font-size-12">
                                    <FaCheckCircle className='rule-icon font-size-14' />
                                    <div className="rule-message">Gồm ký tự in hoa, in thường và số</div>
                                </div>
                                <div className="rule-item display-flex font-size-12">
                                    <FaCheckCircle className='rule-icon font-size-14' />
                                    <div className="rule-message">Gồm ký tự đặc biệt</div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <Form.Item className='form-btn display-flex-center justify-content-center border-rd-8'>
                        <Button onClick={handleClick} className='btn-submit background-none border-none full-width display-flex-center justify-content-center' type="primary" htmlType="submit">
                            Đăng ký
                        </Button>
                    </Form.Item>
                </Form>
            </div>  
            <div className="content-footer">
                Khi bấm "Đăng ký", bạn đã đồng ý với 
                <a href="#"> Điều khoản dịch vụ </a>
                &
                <a href="#"> Chính sách bảo mật </a>
                của Muakey.com
            </div>
        </>
    )
}

export default Step3;