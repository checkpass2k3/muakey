import "../../web/layouts/styles/footer.scss"
import {Row,Col,Input,Button} from "antd"
import tiktokImg from "../../../assets/images/tiktok.svg"
import faceImg from "../../../assets/images/face.svg"
import youtubeImg from "../../../assets/images/youtube.svg"
import instaImg from "../../../assets/images/insta.svg"
import discordImg from "../../../assets/images/discord.svg"
import twitterImg from "../../../assets/images/twitter.svg"
import wechatImg from "../../../assets/images/wechat.svg"
import vietcombankImg from "../../../assets/images/vietcombank.svg"
import payoneerImg from "../../../assets/images/payoneer.svg"
import paypalImg from "../../../assets/images/paypal.svg"
import momoImg from "../../../assets/images/momo.svg"
import usdtImg from "../../../assets/images/usdt.svg"
import bitcoinImg from "../../../assets/images/bitcoin.svg"

const Footer = () => {
  return (
    <div className="footer">
      <div className="footer-container">
        <Row>
          <Col md={17}>
            <div className="menu-link">
              <div ><a href="">Giới thiệu</a></div>
              <div ><a href="">Điều khoản dịch vụ</a></div>
              <div ><a href="">Chính sách bảo mật</a></div>
              <div >
                <a href="">Đăng ký</a>
                <span>/</span>
                <a href="">Đăng nhập</a>
              </div>
            </div>
            <div className="hotline">
              <span>Hotline:</span>
              <a href="">0982636800</a>
             <span className="text-gray">(Các ngày trong tuần từ 8h đến 24h)</span>
            </div>
            <div className="email">
              <span>Email:</span>
              <a href="">hotro@muakey.com</a>
            </div>
            <div className="social">
              <a href=""><img src={faceImg} alt="" /></a>
              <a href=""><img src={youtubeImg} alt="" /></a>
              <a href=""><img src={tiktokImg} alt="" /></a>
              <a href=""><img src={instaImg} alt="" /></a>
              <a href=""><img src={twitterImg} alt="" /></a>
              <a href=""><img src={discordImg} alt="" /></a>
              <a href=""><img src={wechatImg} alt="" /></a>
            </div>
            <p className="text-gray">Copyright Muakey.com 2022, all rights reserved </p>
          </Col>
          <Col md={7}>
            <p>Phương thức thanh toán</p>
            <div className="payment">
              <div className="payment-icon"><img src={vietcombankImg} alt="" /></div>
              <div className="payment-icon"><img src={payoneerImg} alt="" /></div>
              <div className="payment-icon"><img src={paypalImg} alt="" /></div>
              <div className="payment-icon"><img src={momoImg} alt="" /></div>
              <div className="payment-icon"><img src={usdtImg} alt="" /></div>
              <div className="payment-icon"><img src={bitcoinImg} alt="" /></div>
            </div>
            <p>Nhập Email để trở thành fan cứng!</p>
            <Input.Group compact>
                <Input className="input-width" placeholder="Nhập Email của bạn" />
                <Button type="primary">Theo dõi</Button>
            </Input.Group>
          </Col>
        </Row>
      </div>
    </div>
  )
}

export default Footer;
