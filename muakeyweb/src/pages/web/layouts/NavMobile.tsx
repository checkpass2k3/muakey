import "./styles/navmobile.scss"
import { UnorderedListOutlined,UserOutlined } from "@ant-design/icons"
import groupGame from "../../../assets/images/group.png"
import warehouse from "../../../assets/images/warehouse.svg"

const NavMobile = () => {
  return (
    <div className="nav-mobile-cotainer">
      <div className='nav-mobile'>
          <div className="nav-mobile-item">
            <div className="center">
            <div className="nav-mobile-item__image">
            <UnorderedListOutlined />
            </div>
            </div>
            <p>Danh mục</p>
          </div>
          <div className="nav-mobile-item">
            <div className="center">
            <div className="nav-mobile-item__image">
              <img src={groupGame} alt="" />
            </div>
            </div>
            <p>Group Game</p>
          </div>
          <div className="nav-mobile-item">
            <div className="center">
            <div className="nav-mobile-item__image">
            <img src={warehouse} alt="" />
            </div>
            </div>
            <p>Kho hàng</p>
          </div>
          <div className="nav-mobile-item">
            <div className="center">
            <div className="nav-mobile-item__image">
            <UserOutlined />
            </div>
            </div>
            <p>Tài khoản</p>
          </div>
      </div>
    </div>
  )
}

export default NavMobile