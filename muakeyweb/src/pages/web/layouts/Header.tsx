import { Button, Col, Input, Row, Select,Modal,Avatar,Popover  } from 'antd';
import logo from '../../../assets/images/logo.png'
import coin from "../../../assets/images/coin.png"
import avatar from "../../../assets/images/avatar.jpg"
import wareHouse from "../../../assets/images/warehouse.svg"
import './styles/header.scss';
import { DownOutlined, ShoppingCartOutlined, UserOutlined,BellOutlined } from '@ant-design/icons'
import { Link } from 'react-router-dom';
import { useEffect,useState } from 'react';
import PopoverPersonal from './PopoverPersonal';
import PopoverNoti from './PopoverNoti';
import PopoverCart from './PopoverCart';

const Header = () => {
  const { Option } = Select;

  let isFull = false;

  const header = document.querySelector('.header');

  window.addEventListener('scroll', () => {
    if(document.documentElement.scrollTop > 1) {
      header?.classList.add('sticky');
    } else if (!isFull) {
      header?.classList.remove('sticky');
    }
  })

  const [open, setOpen] = useState(false);
  const [openCart, setOpenCart] = useState(false);
  const [openNoti, setOpenNoti] = useState(false);

  const handleOpenChange = (newOpen: boolean) => {
    setOpen(newOpen);
  };

  const menuLink = document.querySelectorAll('div.header-menu-link');

  for (let i = 0; i < menuLink.length; i++) {
    console.log(menuLink[i]);
    menuLink[i].addEventListener('mouseover', () => {
      isFull = true;
      header?.classList.add('sticky');
    }) 
    menuLink[i].addEventListener('mouseout', () => {
      header?.classList.remove('sticky');
      isFull = false;
    })
  }

  return (
    // <div className="header">
    //   <div className="container">
    //     <Row align="middle">
    //       <Col className="display-flex gap-1" span={18}>
    //         <div className="logo">
    //           <a href="/"><img src={logo} alt="logo" /></a>
    //         </div>          
    //       </Col>
    //       <Col md={6} xs={12} className="flex-end mobile-left-0"> 
    //         <div className='gap-1 display-flex'>
    //           <div className='header-right'>
    //               <div className='header-right-image'>
    //                 <img src={coin} alt="" />
    //               </div>
    //               <div className='money'>0 đ</div>
    //               <div className='header-right-image'>
    //                 <a href=""><img src={wareHouse} alt="" /></a>
    //               </div>
    //               <div className='header-right-image'>
    //                 <BellOutlined className='hover-icon' style={{fontSize: "20px",color: "rgb(159 159 159)"}} />
    //               </div>
    //           </div>
    //           <div className="display-flex align-items-center cart-icon">
    //             <ShoppingCartOutlined className='hover-icon' style={{ fontSize: '20px',color: "rgb(159 159 159)" }} />
    //             <p>0</p>
    //           </div>
    //           <div className="hidden-element display-flex">
    //             <div className="account-login">
    //               <div className="personal display-flex">
    //                 <div className='avatar'>
    //                   <img src="" alt="" />
    //                 </div>
    //                 <div className='center'>Name user</div>
    //               </div>
                 
    //               {/* <div className='icon-account'>
    //                 <UserOutlined style={{ fontSize: '15px' }} />
    //               </div>
    //               <Link to="/web/login">Đăng nhập</Link>
    //               <span>/</span>
    //               <Link to="/web/register">Đăng ký</Link> */}
    //             </div>
    //           </div>
    //         </div>
    //         <div className="find full-width">
    //           <Input.Group compact>
    //             <Select defaultValue="Zhejiang" style={{
    //               borderTopLeftRadius: '4px',
    //               borderBottomLeftRadius: '4px'
    //             }}>
    //               <Option value="Zhejiang">All</Option>
    //               <Option value="Jiangsu">Jiangsu</Option>
    //             </Select>
    //             <Input style={{ width: '50%' }} placeholder="Tìm kiếm skins..." />
    //             <Button type="primary">Tìm kiếm</Button>
    //           </Input.Group>
    //         </div>
    //       </Col>
    //       <Col span={6}>
    //         <Row className='gap-1 font-size-24' justify='end'>
    //           <Col><ShoppingCartOutlined className='m-r-1 cursor-pointer'/></Col>
    //           <Col>
    //             <div className="account-login">
    //               <UserOutlined className='font-size-20' style={{ marginTop: '-4px' }}/>
    //               <Link to="/web/login">Đăng nhập</Link>
    //               <span>/</span>
    //               <Link to="/web/register">Đăng ký</Link>
    //             </div>
    //           </Col>
    //         </Row>
    //       </Col>
    //     </Row>
    //   </div>
    //   <div className="header-menu">
    //     <div className="container">
    //       <div className="header-menu-items">
    //         <div className="header-menu-item"><a href="/categories" className="header-menu-link">Tất cả danh mục</a></div>
    //         <div className="header-menu-item">
    //           <div className="header-menu-link">
    //             <div className="d-flex align-items-center pointer">
    //               <div>Game PC</div>
    //               <DownOutlined style={{ marginLeft: '4px' }} />
    //             </div>
    //             <div className="submenu">
    //               <div className="container">
    //                 <div className="row row-cs">
    //                   <div className="col-cs col-md-2-4 mb-2 col">
    //                     <div className="parent-item mb-2">Nền tảng</div> <a href="/categories/battlenet/products" className="submenu-item">Game Battle.net</a><a href="/categories/game-steam-khac/products" className="submenu-item">Game Steam</a><a href="/categories/game-xbox-live/products" className="submenu-item">Game Xbox</a><a href="/categories/game-may-tinh" className="submenu-item">Game PSN</a><a href="/categories/game-may-tinh" className="submenu-item">Game Nintendo Switch</a><a href="/categories/game-origin-khac/products" className="submenu-item">Game Orgin</a><a href="/categories/epic-games/products" className="submenu-item">Game Epic</a><a href="/categories/gogcom/products" className="submenu-item">Game GoG</a><a href="/categories/windows-store/products" className="submenu-item">Game Windows Store</a><a href="/categories/escape-from-tarkov/products" className="submenu-item">Game Escape From Tarkov</a><a href="/categories/fortnite/products" className="submenu-item">Game Fortnite</a><a href="/categories/minecraft/products" className="submenu-item">Game Minecraft</a>
    //                   </div>
    //                   <div className="col-cs col-md-2-4 mb-2 col">
    //                     <div className="parent-item mb-2">Thể loại thích hợp</div>
    //                   </div>
    //                   <div className="col-cs col-md-2-4 mb-2 col">
    //                     <div className="parent-item mb-2">Khám phá theo bộ sưu tập</div>
    //                   </div>
    //                   <div className="col-cs col-md-2-4 mb-2 col">
    //                     <div className="parent-item mb-2">Khám phá theo giá</div>
    //                   </div>
    //                   <div className="col-cs col-md-2-4 mb-2 col">
    //                     <div className="parent-item mb-2">Khá phá theo mức độ phổ biến</div>
    //                   </div>
    //                 </div>
    //               </div>
    //             </div>
    //           </div>
    //         </div>
    //         <div className="header-menu-item"><a href="/categories/gift-card" className="header-menu-link">Gift Cards</a></div>
    //         <div className="header-menu-item"><a href="/categories/game-mobile" className="header-menu-link">Game Mobile Top Up</a></div>
    //         <div className="header-menu-item"><a href="/categories/pubg-pc" className="header-menu-link">PUBG</a></div>
    //         <div className="header-menu-item"><a href="/categories/tai-khoan-tien-ich" className="header-menu-link">Tài khoản tiện ích</a></div>
    //         <div className="header-menu-item"><a href="/" aria-current="page" className="header-menu-link nuxt-link-exact-active nuxt-link-active">Tin Tức Game</a></div>
    //       </div>
    //     </div>
    //   </div>
    // </div>
    <div className="header">
      <div className="header-container">
        <Row align="middle">
          <Col className="display-flex gap-1" md={3} xs={12}>
            <div className="logo">
              <a href="/"><img src={logo} alt="logo" /></a>
            </div>          
          </Col>
          <Col md={6} xs={12} className="flex-end mobile-left-0"> 
            <div className='gap-1 display-flex'>
              <div className='header-right'>
                  <div className='header-right-image'>
                    <img src={coin} alt="" />
                  </div>
                  <div className='money'>0 đ</div>
                  <div className='header-right-image'>
                    <a href=""><img src={wareHouse} alt="" /></a>
                  </div>
                 
                  <Popover
                    content={<PopoverNoti />}
                    trigger="click"
                    open={openNoti}
                    onOpenChange={() => setOpenNoti(!openNoti)}
                  >
                     <div className='header-right-image'>
                      <BellOutlined className='hover-icon' style={{fontSize: "20px",color: "rgb(159 159 159)"}} />
                    </div>
                  </Popover>
              </div>
              <Popover
                    content={<PopoverCart />}
                    trigger="click"
                    open={openCart}
                    onOpenChange={() => setOpenCart(!openCart)}
                  >
                    <div className="display-flex align-items-center cart-icon">
                      <ShoppingCartOutlined className='hover-icon' style={{ fontSize: '20px',color: "rgb(159 159 159)" }} />
                      <p>0</p>
                    </div>
                  </Popover>
              
              <div className="hidden-element display-flex">
                <div className="account-login">
                  
                  <Popover
                    content={<PopoverPersonal />}
                    trigger="click"
                    open={open}
                    onOpenChange={handleOpenChange}
                  >
                    <div className="personal display-flex">
                      <div className='avatar'>
                        <img src={avatar} alt="" />
                      </div>
                      <div className='center'>Name user</div>
                  </div>
                  </Popover>
                  {/* <div className='icon-account'>
                    <UserOutlined style={{ fontSize: '15px' }} />
                  </div>
                  <Link to="/web/login">Đăng nhập</Link>
                  <span>/</span>
                  <Link to="/web/register">Đăng ký</Link> */}
                </div>
              </div>
            </div>
          </Col>
          <Col md={15} xs={24} className="mobile-right-0">
            <div className="find full-width">
              <Input.Group compact className='header-search'>
                <Select defaultValue="Zhejiang" style={{
                  borderTopLeftRadius: '4px',
                  borderBottomLeftRadius: '4px'
                }}>
                  <Option value="Zhejiang">All</Option>
                  <Option value="Jiangsu">Jiangsu</Option>
                </Select>
                <Input style={{ width: '45%' }} placeholder="Tìm kiếm Skins..." />
                <Button type="primary">Tìm kiếm</Button>
              </Input.Group>
            </div>
          </Col>
        </Row>
        
      </div>
      <div className="header-menu">
          <div className="header-container">
            <div className="header-menu-items">
              <div className="header-menu-item"><a href="/categories" className="header-menu-link">Tất cả danh mục</a></div>
              <div className="header-menu-item">
                <div className="header-menu-link">
                  <div className="d-flex align-items-center pointer">
                    <div>Game PC</div>
                    <DownOutlined style={{fontSize: "10px",marginLeft: "5px"}} />
                  </div>
                  <div className="submenu">
                    <div className="container">
                      <div className="row row-cs">
                        <div className="col-cs col-md-2-4 mb-2 col">
                          <div className="parent-item mb-2">Nền tảng</div> <a href="/categories/battlenet/products" className="submenu-item">Game Battle.net</a><a href="/categories/game-steam-khac/products" className="submenu-item">Game Steam</a><a href="/categories/game-xbox-live/products" className="submenu-item">Game Xbox</a><a href="/categories/game-may-tinh" className="submenu-item">Game PSN</a><a href="/categories/game-may-tinh" className="submenu-item">Game Nintendo Switch</a><a href="/categories/game-origin-khac/products" className="submenu-item">Game Orgin</a><a href="/categories/epic-games/products" className="submenu-item">Game Epic</a><a href="/categories/gogcom/products" className="submenu-item">Game GoG</a><a href="/categories/windows-store/products" className="submenu-item">Game Windows Store</a><a href="/categories/escape-from-tarkov/products" className="submenu-item">Game Escape From Tarkov</a><a href="/categories/fortnite/products" className="submenu-item">Game Fortnite</a><a href="/categories/minecraft/products" className="submenu-item">Game Minecraft</a>
                        </div>
                        <div className="col-cs col-md-2-4 mb-2 col">
                          <div className="parent-item mb-2">Thể loại thích hợp</div>
                        </div>
                        <div className="col-cs col-md-2-4 mb-2 col">
                          <div className="parent-item mb-2">Khám phá theo bộ sưu tập</div>
                        </div>
                        <div className="col-cs col-md-2-4 mb-2 col">
                          <div className="parent-item mb-2">Khám phá theo giá</div>
                        </div>
                        <div className="col-cs col-md-2-4 mb-2 col">
                          <div className="parent-item mb-2">Khá phá theo mức độ phổ biến</div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div className="header-menu-item">
                <div className="header-menu-link">
                  <div className="d-flex align-items-center pointer">
                    <div>Gift Card</div>
                    <DownOutlined style={{fontSize: "10px",marginLeft: "5px"}} />
                  </div>
                  <div className="submenu">
                    <div className="container">
                      <div className="row row-cs">
                        <div className="col-cs col-md-2-4 mb-2 col">
                          <div className="parent-item mb-2">Nền tảng</div> <a href="/categories/battlenet/products" className="submenu-item">Game Battle.net</a><a href="/categories/game-steam-khac/products" className="submenu-item">Game Steam</a><a href="/categories/game-xbox-live/products" className="submenu-item">Game Xbox</a><a href="/categories/game-may-tinh" className="submenu-item">Game PSN</a><a href="/categories/game-may-tinh" className="submenu-item">Game Nintendo Switch</a><a href="/categories/game-origin-khac/products" className="submenu-item">Game Orgin</a><a href="/categories/epic-games/products" className="submenu-item">Game Epic</a><a href="/categories/gogcom/products" className="submenu-item">Game GoG</a><a href="/categories/windows-store/products" className="submenu-item">Game Windows Store</a><a href="/categories/escape-from-tarkov/products" className="submenu-item">Game Escape From Tarkov</a><a href="/categories/fortnite/products" className="submenu-item">Game Fortnite</a><a href="/categories/minecraft/products" className="submenu-item">Game Minecraft</a>
                        </div>
                        <div className="col-cs col-md-2-4 mb-2 col">
                          <div className="parent-item mb-2">Thể loại thích hợp</div>
                        </div>
                        <div className="col-cs col-md-2-4 mb-2 col">
                          <div className="parent-item mb-2">Khám phá theo bộ sưu tập</div>
                        </div>
                        <div className="col-cs col-md-2-4 mb-2 col">
                          <div className="parent-item mb-2">Khám phá theo giá</div>
                        </div>
                        <div className="col-cs col-md-2-4 mb-2 col">
                          <div className="parent-item mb-2">Khá phá theo mức độ phổ biến</div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div className="header-menu-item">
                <div className="header-menu-link">
                  <div className="d-flex align-items-center pointer">
                    <div>Nạp Game</div>
                    <DownOutlined style={{fontSize: "10px",marginLeft: "5px"}} />
                  </div>
                  <div className="submenu">
                    <div className="container">
                      <div className="row row-cs">
                        <div className="col-cs col-md-2-4 mb-2 col">
                          <div className="parent-item mb-2">Nền tảng</div> <a href="/categories/battlenet/products" className="submenu-item">Game Battle.net</a><a href="/categories/game-steam-khac/products" className="submenu-item">Game Steam</a><a href="/categories/game-xbox-live/products" className="submenu-item">Game Xbox</a><a href="/categories/game-may-tinh" className="submenu-item">Game PSN</a><a href="/categories/game-may-tinh" className="submenu-item">Game Nintendo Switch</a><a href="/categories/game-origin-khac/products" className="submenu-item">Game Orgin</a><a href="/categories/epic-games/products" className="submenu-item">Game Epic</a><a href="/categories/gogcom/products" className="submenu-item">Game GoG</a><a href="/categories/windows-store/products" className="submenu-item">Game Windows Store</a><a href="/categories/escape-from-tarkov/products" className="submenu-item">Game Escape From Tarkov</a><a href="/categories/fortnite/products" className="submenu-item">Game Fortnite</a><a href="/categories/minecraft/products" className="submenu-item">Game Minecraft</a>
                        </div>
                        <div className="col-cs col-md-2-4 mb-2 col">
                          <div className="parent-item mb-2">Thể loại thích hợp</div>
                        </div>
                        <div className="col-cs col-md-2-4 mb-2 col">
                          <div className="parent-item mb-2">Khám phá theo bộ sưu tập</div>
                        </div>
                        <div className="col-cs col-md-2-4 mb-2 col">
                          <div className="parent-item mb-2">Khám phá theo giá</div>
                        </div>
                        <div className="col-cs col-md-2-4 mb-2 col">
                          <div className="parent-item mb-2">Khá phá theo mức độ phổ biến</div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div className="header-menu-item"><a href="/categories/pubg-pc" className="header-menu-link">PUBG</a></div>
              <div className="header-menu-item"><a href="/categories/tai-khoan-tien-ich" className="header-menu-link">Tài khoản tiện ích</a></div>
              <div className="header-menu-item"><a href="/" aria-current="page" className="header-menu-link nuxt-link-exact-active nuxt-link-active">Tin Tức Game</a></div>
            </div>
          </div>
        </div>
    </div>
  )
}

export default Header;
