import './styles/contact.scss'
import spaceship from "../../../assets/images/spaceship-icon.png"
import groupGame from "../../../assets/images/group.png"
import message from "../../../assets/images/message.png"

const Contact = () => {
  return (
    <>
    <div className="message">
        <div className='message-image'>
        <img src={message} alt="" />
        </div>
    </div>
    <div className='contact'>
        
        <div className="contact-item" onClick={() =>window.scrollTo(0, 0)}>
            <div className='center'>
                <div className='contact-item-image'>
                    <img src={spaceship} alt="" />
                </div>
            </div>
            
            <p>Đầu trang</p>
        </div>
        <div className="contact-item group-game">
            <div className='center'>
                    <a href='' className='contact-item-image'>
                        <img src={groupGame} alt="" />
                    </a>
                </div>
                <p>Group Game</p>
            </div>
        </div>
    </>
  )
}

export default Contact