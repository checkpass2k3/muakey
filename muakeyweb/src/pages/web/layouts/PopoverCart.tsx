import "./styles/popover.scss"
import arknight from "../../../assets/images/arknights.jpg"

const PopoverCart = () => {
  return (
    <div className="popover-cart">
        <h1>Giỏ hàng</h1>
        <div className="cart">
        <div className="cart-item display-flex">
                <img src={arknight} alt="" />
                <div className="cart-item-right">
                    <div className="name">40 Kim Cương - 30$ Arknights</div>
                    <div className="line2">

                    </div>
                    <div className="flex-justify-between">
                        <div className="quantity">
                            <span>-</span>
                            1
                            <span>+</span>
                            
                        </div>
                        <div className="sold">585.000đ</div>
                    </div>
                </div>
            </div>
            <div className="cart-item display-flex">
                <img src={arknight} alt="" />
                <div className="cart-item-right">
                    <div className="name">40 Kim Cương - 30$ Arknights</div>
                    <div className="line2">

                    </div>
                    <div className="flex-justify-between">
                        <div className="quantity">
                            <span>-</span>
                            1
                            <span>+</span>
                            
                        </div>
                        <div className="sold">585.000đ</div>
                    </div>
                </div>
            </div>
            <div className="cart-item display-flex">
                <img src={arknight} alt="" />
                <div className="cart-item-right">
                    <div className="name">40 Kim Cương - 30$ Arknights</div>
                    <div className="line2">

                    </div>
                    <div className="flex-justify-between">
                        <div className="quantity">
                            <span>-</span>
                            1
                            <span>+</span>
                            
                        </div>
                        <div className="sold">585.000đ</div>
                    </div>
                </div>
            </div>
            <div className="cart-item display-flex">
                <img src={arknight} alt="" />
                <div className="cart-item-right">
                    <div className="name">40 Kim Cương - 30$ Arknights</div>
                    <div className="line2">

                    </div>
                    <div className="flex-justify-between">
                        <div className="quantity">
                            <span>-</span>
                            1
                            <span>+</span>
                            
                        </div>
                        <div className="sold">585.000đ</div>
                    </div>
                </div>
            </div>
            <div className="cart-item display-flex">
                <img src={arknight} alt="" />
                <div className="cart-item-right">
                    <div className="name">40 Kim Cương - 30$ Arknights</div>
                    <div className="line2">

                    </div>
                    <div className="flex-justify-between">
                        <div className="quantity">
                            <span>-</span>
                            1
                            <span>+</span>
                            
                        </div>
                        <div className="sold">585.000đ</div>
                    </div>
                </div>
            </div>
        </div>
        <div className="total-money flex-justify-between">
            <p>Tổng tiền</p>
            <span className="sold">762.000đ</span>
        </div>
    </div>
  )
}

export default PopoverCart