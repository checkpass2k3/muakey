import "./styles/popover.scss"
import { RightOutlined } from "@ant-design/icons"
import goldRank from "../../../assets/images/gold-rank.svg"
import per from "../../../assets/images/personal.svg"
import currency from "../../../assets/images/currency-dollar.svg"
import warehouse from "../../../assets/images/warehouse.svg"
import recharge from "../../../assets/images/recharge-history.svg"
import transaction from "../../../assets/images/transaction-history.svg"
import favorite from "../../../assets/images/favorite.svg"
import logout from "../../../assets/images/logout.svg"

const PopoverPersonal = () => {
  return (
    <div className="popover-personal">
        <div className="display-flex justify-content-between">
            <div>
                <span style={{marginRight: "5px",textTransform:"uppercase"}}>Rookie</span>
                <RightOutlined />
            </div>
            <img src={goldRank} alt="" />
        </div>
        <div className="link-personal display-flex justify-content-evenly">
            <div className="link-personal-item">
                <img src={per} alt="" />
                <p>Tài khoản của tôi</p>
            </div>
            <div className="link-personal-item">
                <img src={currency} alt="" />
                <p>Nạp tiền</p>
            </div>
            <div className="link-personal-item">
                <img src={warehouse} alt="" />
                <p>Kho hàng</p>
            </div>
            <div className="link-personal-item">
                <img src={recharge} alt="" />
                <p>Lịch sử nạp tiền</p>
            </div>
            <div className="link-personal-item">
                <img src={transaction} alt="" />
                <p>Lịch sử giao dịch</p>
            </div>
            <div className="link-personal-item">
                <img src={favorite} alt="" />
                <p>Sản phẩm yêu thích</p>
            </div>
            <div className="link-personal-item">
                <img src={logout} alt="" />
                <p>Đăng xuất</p>
            </div>
        </div>
    </div>
  )
}

export default PopoverPersonal