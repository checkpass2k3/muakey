import "../styles/cartbodyproduct.scss"
import { RestOutlined } from "@ant-design/icons"
import arknight from "../../../../assets/images/arknights.jpg"
import {Checkbox} from 'antd'

const CartBodyProduct = () => {
  return (
    <div className="cart-body-product">
        <div className="cart-body-product__item">
            <div className="cart-body-product__item--left">
            <Checkbox className="center"></Checkbox>
                <img src={arknight} alt="" />
                <div className="center">
                    <div className="product-name">
                        <p>PUBG PC PLus + Loại 3: Tài khoản Level từ 30 - 79</p>
                        <button>order</button>
                    </div>
                </div>
                
            </div>
            <div className="cart-body-product__item--right">
                <div className="center">
                    <div className="price">399.000đ</div>
                    <div className="increase-decrease-quantity display-flex">
                        <span>-</span>
                        <div>1</div>
                        <span>+</span>
                    </div>
                    <RestOutlined style={{fontSize: "20px",cursor:"pointer"}} />
                </div>
            </div>
            
        </div>
        <div className="cart-body-product__item">
            <div className="cart-body-product__item--left">
            <Checkbox className="center"></Checkbox>
                <img src={arknight} alt="" />
                <div className="center">
                    <div className="product-name">
                        <p>PUBG PC PLus + Loại 3: Tài khoản Level từ 30 - 79</p>
                        <button>order</button>
                    </div>
                </div>
                
            </div>
            <div className="cart-body-product__item--right">
                <div className="center">
                    <div className="price">399.000đ</div>
                    <div className="increase-decrease-quantity display-flex">
                        <span>-</span>
                        <div>1</div>
                        <span>+</span>
                    </div>
                    <RestOutlined style={{fontSize: "20px",cursor:"pointer"}} />
                </div>
            </div>
            
        </div>
    </div>
  )
}

export default CartBodyProduct