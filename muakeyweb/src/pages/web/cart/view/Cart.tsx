import "../styles/cart.scss"
import shoppingEmpty from "../../../../assets/images/shopping-cart-empty.svg"
import {Button} from "antd"
import { useState } from "react"
import CartBodyProduct from "../components/CartBodyProduct"

const Cart = () => {

    const [itemCart,setItemCart] = useState<number>(1)

  return (
    <div className="cart">
        <div className="cart-head">
            <div className="cart-head-word">Giỏ hàng</div>
            <p>0 sản phẩm đã được chọn</p>
        </div>
        <div className={itemCart > 0 ?"cart-body":"cart-body center"}>
            <CartBodyProduct />
            <div className="cart-body-payment">
                <div className="cart-body-payment__center">
                    <div className="shared">
                        <p>Giá gốc</p>
                        <span>0đ</span>
                    </div>
                    <div className="shared">
                        <p>Giảm giá</p>
                        <span>0đ(0%)</span>
                    </div>
                    <div className="line"></div>
                    <div className="shared">
                        <p>Tổng tiền</p>
                        <span>0đ</span>
                    </div>
                    <Button disabled>Thanh toán</Button>
                </div>
            </div>
            {/* <div className="shopping-empty" style={{textAlign:"center"}}>
                <img src={shoppingEmpty} alt="" />
                <div className="text-cart-empty">Giỏ hàng của bạn đang trống</div>
                <p>Mua gì đi bạn ơi !!!</p>
            </div> */}
            
        </div>
    </div>
  )
}

export default Cart