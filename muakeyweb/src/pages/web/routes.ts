import React from 'react';

const Home = React.lazy(() => import('./home/view/Home'));
const Default = React.lazy(() => import('./auth/views/Default'));
const Page404 = React.lazy(() => import('../../layouts/Page404'));
const Step4 = React.lazy(() => import('./auth/views/Step4'));
const Category = React.lazy(() => import('./categories/view/Category'));
const CategoryPoduct = React.lazy(() => import('./categories/view/CategoryProduct'));
const DetailPoduct = React.lazy(() => import('./detail/view/DetailProduct'));
const Cart = React.lazy(() => import('./cart/view/Cart'));
const Checkout = React.lazy(() => import('./checkout/view/Checkout'));
const Notification = React.lazy(() => import('./notification/view/Notification'));
const Account = React.lazy(() => import('./account/view/Account'));
// const AccountProfile = React.lazy(() => import('./account/view/AccountProfile'));
// const BookingDetail = React.lazy(() => import('./views/BookingDetail'));

const routes: any[] = [
  { path: '/', index: true, name: 'Home', component: Home },
  { path: '/login', index: true, name: 'Login', component: Default },
  { path: '/notification', index: true, name: 'Notification', component: Notification },
  { path: '/register', index: true, name: 'Register', component: Default },
  { path: '/register-success', index: true, name: 'Success', component: Step4 },
  // { path: '*', exact: true, name: 'Page404', component: Page404 },
  { path: '/categories', index: true, name: 'Category', component: Category },
  { path: '/categories/:slugCate', index: true, name: 'CategoryPoduct', component: CategoryPoduct },
  { path: '/chi-tiet', index: true, name: 'DetailPoduct', component: DetailPoduct },
  { path: '/cart', index: true, name: 'Cart', component: Cart },
  { path: '/cart/checkout', index: true, name: 'Checkout', component: Checkout },
  { path: '/account/', index: true, name: 'Account', component: Account },
  // { path: '/account/profile', index: false, name: 'AccountProfile', component: AccountProfile },
  // { path: '/detail/:id', index: true, name: 'Booking', component: BookingDetail },
];

export default routes;
