import "../styles/tabcontentpayment.scss";
import coin from "../../../../assets/images/coin.png"
import vietcombankPay from "../../../../assets/images/vietcombank-payment.svg"
import momo from "../../../../assets/images/momo.svg"
import viettel from "../../../../assets/images/viettel-payment.svg"
import mobiphone from "../../../../assets/images/mobiphone.png"
import vinaphone from "../../../../assets/images/vinaphone.svg"
import vietnammobile from "../../../../assets/images/vietnammobile.svg"
import garena from "../../../../assets/images/garena.svg"
import zing from "../../../../assets/images/zing.svg"
import {AlertFilled} from "@ant-design/icons"

const TabContentPayment = ({id}:any) => {
  return (
    <div className="tab-content-payment">
        <div className="tab-content-payment-center">
            {id===1 && <>
            <div className="text-title">Bằng số dư</div>
                <div className="money-to-pay">
                    <div className="money-to-pay-content">
                        <p>Bạn phải trả: 727.000đ</p>
                        <span >Số dư không đủ, vui lòng nạp thêm tiền. Đi đến trang </span><a href="">Nạp tiền</a>
                    </div>
                </div>
            <div className="muakey-wallet">
                <div className="muakey-wallet-image">
                    <img src={coin} alt="" />
                </div>
                <div className="muakey-wallet-text">
                    <p>Muakey Wallet</p>
                    <span>Số dư hiện tại: 0đ</span>
                </div>
            </div>
            </>}
            {id===2 && <>
                <div className="text-title">Tài khoản ngân hàng</div>
                <div className="money-to-pay">
                    <div className="bank-pay-content selected-item center">
                        <div className="">
                            <div className="center">
                                <img src={vietcombankPay} alt="" />
                            </div>
                            <div style={{textAlign:"center"}}>
                                <p>Vietcombank</p>
                                <span >Ck 0%</span>
                            </div>
                        </div>
                    </div>
                </div>
            </>}
            {id===3 && <>
                <div className="text-title">Ví điện tử Momo, Zalo Pay,...</div>
                <div className="money-to-pay">
                    <div className="bank-pay-content selected-item center">
                        <div className="">
                            <div className="center">
                                <img src={momo} alt="" />
                            </div>
                            <div style={{textAlign:"center"}}>
                                <p>Momo</p>
                                <span >Ck 0%</span>
                            </div>
                        </div>
                    </div>
                </div>
            </>}
            {id ===4 && <>
                <div className="text-title">Thẻ cào</div>
                <div className="card-pay">
                    <div className="card-pay-content">
                        <div className="card-pay-content-item">
                        <div className="center">
                                <img src={viettel} alt="" />
                            </div>
                            <div style={{textAlign:"center"}}>
                                <p>Viettel</p>
                                <span >Ck 32%</span>
                            </div>
                        </div>
                        <div className="card-pay-content-item">
                        <div className="center">
                                <img src={mobiphone} alt="" />
                            </div>
                            <div style={{textAlign:"center"}}>
                                <p>Mobiphone</p>
                                <span >Ck 32%</span>
                            </div>
                        </div>
                        <div className="card-pay-content-item">
                        <div className="center">
                                <img src={vinaphone} alt="" />
                            </div>
                            <div style={{textAlign:"center"}}>
                                <p>Vinaphone</p>
                                <span >Ck 32%</span>
                            </div>
                        </div>
                        <div className="card-pay-content-item">
                        <div className="center">
                                <img src={vietnammobile} alt="" />
                            </div>
                            <div style={{textAlign:"center"}}>
                                <p>Vietnammobile</p>
                                <span >Ck 32%</span>
                            </div>
                        </div>
                        <div className="card-pay-content-item">
                        <div className="center">
                                <img src={garena} alt="" />
                            </div>
                            <div style={{textAlign:"center"}}>
                                <p>Garena</p>
                                <span >Ck 32%</span>
                            </div>
                        </div>
                        <div className="card-pay-content-item">
                        <div className="center">
                                <img src={zing} alt="" />
                            </div>
                            <div style={{textAlign:"center"}}>
                                <p>Zing</p>
                                <span >Ck 32%</span>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="note">
                    <div className="note-content display-flex">
                    <AlertFilled />
                    <div style={{marginLeft: "10px"}}>
                        <p>Bạn phải nạp ít nhất (Tổng tiền + chiết khấu): 727.000đ</p>
                        <span>Bạn vui lòng chuyển sang trang Nạp tiền bằng thẻ cào để thêm số dư vào tài khoản.</span> 
                        <span> Sau đó quay lại trang Giỏ hàng để hoàn tất thanh toán bằng số dư.</span>
                    </div>
                    
                    </div>
                </div>
            </>}
        </div>
    </div>
  )
}

export default TabContentPayment