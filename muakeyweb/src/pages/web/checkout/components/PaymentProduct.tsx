import "../styles/paymentproduct.scss"
import arknight from "../../../../assets/images/arknights.jpg"

const PaymentProduct = () => {
  return (
    <div className="payment-product">
        <div className="payment-product-item">
            <img src={arknight} alt="" />
            <div className="name">
                <p className="name-product">40 Kim Cương - 30$ Arknights</p>
                <div className="flex-justify-between">
                    <p className="quantity">Số lượng: 1</p>
                    <p className="money">585.000đ</p>
                </div>
            </div>
        </div>
        <div className="payment-product-item">
            <img src={arknight} alt="" />
            <div className="name">
                <p className="name-product">40 Kim Cương - 30$ Arknights</p>
                <div className="flex-justify-between">
                    <p className="quantity">Số lượng: 1</p>
                    <p className="money">585.000đ</p>
                </div>
            </div>
        </div>
    </div>
  )
}

export default PaymentProduct