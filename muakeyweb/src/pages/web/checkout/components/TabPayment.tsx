import "../styles/tabpayment.scss"
import key from "../../../../assets/images/key.svg"
import bank from "../../../../assets/images/bank.svg"
import momoPayment from "../../../../assets/images/momo-payment.svg"
import viettel from "../../../../assets/images/viettel.svg"

const TabPayment = ({id}:any) => {
  return (
    <div className="tab-payment">
      {id === 1 && 
      <div className="tab-payment-content">
        <div className="payment-image">
          <img src={key} alt="" />
        </div>
      <div className="text-payment">Bằng số dư</div>
      </div>}
      {id=== 2 && <div className="tab-payment-content">
        <div className="payment-image"><img src={bank} alt="" /></div>
      <div className="text-payment">Tài khoản ngân hàng</div></div>}
      {id=== 3 && <div className="tab-payment-content">
        <div className="payment-image"><img src={momoPayment} alt="" /></div>
      <div className="text-payment">Ví điện tử</div></div>}
      {id=== 4 && <div className="tab-payment-content">
        <div className="payment-image"><img src={viettel} alt="" /></div>
      <div className="text-payment">Thẻ cào</div></div>}
    </div>
  )
}

export default TabPayment