import "../styles/discountcode.scss"
import { SettingFilled } from '@ant-design/icons'
import { Modal,Button } from "antd"
import { useState } from "react"
import discount from "../../../../assets/images/discount.svg"

const DiscountCode = () => {

  const [open, setOpen] = useState(false);

  const showModal = () => {
    setOpen(true);
  };

  const hideModal = () => {
    setOpen(false);
  };

  return (
    <>
        <p>Mã giảm giá</p>
        <div className="code-discount" onClick={showModal}>
            <SettingFilled />
            <span>Nhập mã giảm giá</span>
        </div>
        <Modal
          open={open}
          onOk={hideModal}
          onCancel={hideModal}
          okText="OK"
          cancelText="Hủy"
        >
          <p>Chọn mã giảm giá</p>
          <div className="enter-discount">
            <input type="text" placeholder="Nhập mã giảm giá..." />
            <button>Áp dụng</button>
          </div>
          
          <p className="code">Mã giảm giá</p>
          <div className="center">
            <div>
              <div className="center">
                <img src={discount} alt="" />
              </div>
              <p>Không có mã giảm giá</p>
            </div>
          </div>
          <span>*Thu thập mã giảm giá tại <a href="">Đổi điểm</a></span>
        </Modal>
    </>
  )
}

export default DiscountCode