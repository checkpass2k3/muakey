import "../styles/checkout.scss"

import PaymentProduct from "../components/PaymentProduct"
import DiscountCode from "../components/DiscountCode"
import type { RadioChangeEvent } from 'antd';
import { Radio, Space, Tabs,Button } from 'antd';
import React, { useState } from 'react';
import TabPayment from "../components/TabPayment"
import TabContentPayment from "../components/TabContentPayment";

type TabPosition = 'left' | 'right' | 'top' | 'bottom';

const Checkout = () => {

const [tabPosition, setTabPosition] = useState<TabPosition>('left');

  const changeTabPosition = (e: RadioChangeEvent) => {
    setTabPosition(e.target.value);
  };

  return (
    <div className="checkout header-container">
        <div className="checkout-head">
            Thanh toán
        </div>
        <div className="checkout-container">
            <div className="checkout-container-left">
            <Tabs
                tabPosition={tabPosition}
                items={new Array(4).fill(null).map((_, i) => {
                const id = String(i + 1);
                const check = i +1;
                return {
                    label: <TabPayment id={check} />,
                    key: id,
                    children: <TabContentPayment id={check} />,
                };
                })}
            />
            </div>
            <div className="checkout-container-right">
                <div className="checkout-container-right__top">
                    <DiscountCode />
                </div>
                <div className="checkout-container-right__bottom">
                    <PaymentProduct />
                    <div className="cost-discount">
                        <div className="cost flex-justify-between">
                            <span>Giá gốc</span>
                            <span>718.000đ</span>
                        </div>
                        <div className="discount flex-justify-between">
                            <span>Giảm giá</span>
                            <span>0đ (0%)</span>
                        </div>
                    </div>
                    <div className="total-money flex-justify-between">
                        <span>Tổng tiền</span>
                        <span className="color-money">718.000đ</span>
                    </div>
                    <Button disabled>Thanh toán</Button>
                </div>
            </div>
        </div>
            
    </div>
  )
}

export default Checkout