import { Button, Carousel } from 'antd';
import banner1 from '../../../../assets/images/banner1.png';
import banner2 from '../../../../assets/images/banner2.png';
import banner3 from '../../../../assets/images/banner3.png';
import '../styles/home.scss';

const Banner = () => {
    return (
        <div className='banner'>
            <Carousel autoplay>
                <div className='banner-container'>
                    <img src={banner1} alt="banner1" />
                    <div className="banner-wrap">
                        <div className="banner-content">
                            <div className='content-heading'>Top up, Nạp game mobile</div>
                            <div className="content-title">Truy cập ngay để nạp game Mobile giá rẻ, Top up Game Moblie</div>
                            <a href="https://www.youtube.com/">
                                <Button type='primary'>Xem thêm</Button>
                            </a>
                        </div>
                    </div>
                </div>
                <div className='banner-container'>
                    <img src={banner2} alt="banner2" />
                    <div className="banner-wrap">
                        <div className="banner-content">
                            <div className='content-heading'>Game bản quyền Steam, Blizzard, Epic Game, EA Game, ...</div>
                            <div className="content-title">Truy cập để sở hữu key game bản quyền giá ưu đãi</div>
                            <a href="https://www.youtube.com/">
                                <Button type='primary'>Xem thêm</Button>
                            </a>
                        </div>
                    </div>
                </div>
                <div className='banner-container'>
                    <img src={banner3} alt="banner3" />
                    <div className="banner-wrap">
                        <div className="banner-content">
                            <div className='content-heading'>PlayerUnknown's Battlegrounds</div>
                            <div className="content-title">Truy cập ngay để nạp Gcoin giá ưu đãi cùng rất nhiều code skin Pubg</div>
                            <a href="https://www.youtube.com/">
                                <Button type='primary'>Xem thêm</Button>
                            </a>
                        </div>
                    </div>
                </div>
                
            </Carousel>
        </div>
    )
}

export default Banner; 