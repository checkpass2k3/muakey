import { Carousel } from "antd";
import category1 from '../../../../assets/images/category/category1.png';
import '../styles/home.scss';

const Category = () => {
    return (
        <div className="category">
            <div className="container">
                <div className="flex-justify-between align-items-center">
                    <div className="font-size-24 font-weight-semibold">Tất Cả Danh Mục</div>
                    <a className="category-discovery " href="https://www.youtube.com/">Xem tất cả</a>
                </div>
                <div className="category-slider">
                    Category Slider
                </div>
            </div>
        </div>
    )
}

export default Category;