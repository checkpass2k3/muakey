import AllCategory from "../components/AllCategory";
import FlashSaleProduct from "../components/FlashSaleProduct";
import GameMobile from "../components/GameMobile";
import GamePC from "../components/GamePC";
import HomeCategoryOne from "../components/HomeCategoryOne";
import ProductTab from "../components/ProductTab";
import Slider from "../components/Slider";
import Utilities from "../components/Utilities";
import "../styles/home.scss"
import GiftCard from "../components/GiftCard";

const Home = () => {
  return (
    <>
      <Slider />
      <AllCategory />
      <FlashSaleProduct />
      <ProductTab />
      <HomeCategoryOne />
      <GamePC title={"Valorant"} />
      <GamePC title={"Game Steam Khác"} />
      <GameMobile />
      <GiftCard />
      <Utilities />
      </>
  )
}

export default Home;

