import ProductTabContent from "./ProductTabContent"
import "../styles/gamepc.scss"
import "../styles/producttab.scss"
import { Tabs } from "antd"

const GamePC = ({title}:any) => {
  return (
    <div className="container-pc">
        <div className="container-pc-title">
            <p>{title}</p>
            <a href="">Xem thêm</a>
        </div>
        <div className="product-tab-container">
          <div className="product-tab-nav tab-item">
            <Tabs type='card'>
              <Tabs.TabPane tab="Hôm nay có gì ?" key="item-1">
                <ProductTabContent />
              </Tabs.TabPane>
              <Tabs.TabPane tab="Gợi ý cho bạn" key="item-2">
                <ProductTabContent />
              </Tabs.TabPane>
              <Tabs.TabPane tab="Bán chạy" key="item-3">
                <ProductTabContent />
              </Tabs.TabPane>
              <Tabs.TabPane tab="Dưới 100k" key="item-4">
                <ProductTabContent />
              </Tabs.TabPane>
              <Tabs.TabPane tab="Đánh giá cao" key="item-5">
                <ProductTabContent />
              </Tabs.TabPane>
              
            </Tabs>
            <a className="view-more" href="">Xem thêm</a>
          </div>
      </div>
    </div>
  )
}

export default GamePC