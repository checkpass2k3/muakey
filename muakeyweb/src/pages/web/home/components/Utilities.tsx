import SyntheticGame from "./SyntheticGame"
import "../styles/ultilities.scss"
import ProductTabNav from "./ProductTabNav"
import ProductTabContent from "./ProductTabContent"
import ProductTab from "./ProductTab"

const Utilities = () => {
  return (
    <div className="ultilities-container">
        <div className="header-container">
            <div className="ultilities">
                <SyntheticGame title={"Tiện ích"} />
            </div>
        </div>
        <ProductTab />
        
    </div>
  )
}

export default Utilities