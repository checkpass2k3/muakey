import {LeftOutlined,RightOutlined } from "@ant-design/icons"
import ProductCategory from "./ProductCategory"
import "../styles/syntheticgame.scss"

const SyntheticGame = ({title}:any) => {
  return (
    <>
        <div className="game-pc-title">
            <div className="game-pc-title__left">
                {title}
            </div>
            <div className="game-pc-title__right">
                <div className="link"><a href="">Xem tất cả</a></div>
                <div className="previous"><LeftOutlined /></div>
                <div className="next"><RightOutlined /></div>
            </div>
        </div>
        <div className="game-pc-content">
            <ProductCategory />
        </div>
        </>
  )
}

export default SyntheticGame