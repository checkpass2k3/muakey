import "../styles/allcategory.scss";
import ImagesSlider from "./ImagesSlider";
import ProductCategory from "./ProductCategory";

const AllCategory = () => {
  return (
    <div className="all-cate">
      <div className="all-cate__head">
        <div className="all-cate__head--title">Tất cả danh mục</div>
        <a className="all-cate__head--viewall" href="">Xem tất cả</a>
      </div>
      <ProductCategory />
      <ImagesSlider />
    </div>
  )
}

export default AllCategory