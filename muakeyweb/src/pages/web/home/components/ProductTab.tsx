import "../styles/producttab.scss"
import ProductTabContent from "./ProductTabContent"
import { Button,Tabs } from "antd"

const ProductTab = () => {
  return (
    <div className="product-tab">
        <div className="product-tab-container">
          <div className="product-tab-nav header-container">
            <Tabs type='card'>
              <Tabs.TabPane tab="Hôm nay có gì ?" key="item-1">
                <ProductTabContent />
              </Tabs.TabPane>
              <Tabs.TabPane tab="Gợi ý cho bạn" key="item-2">
                <ProductTabContent />
              </Tabs.TabPane>
              <Tabs.TabPane tab="Bán chạy" key="item-3">
                <ProductTabContent />
              </Tabs.TabPane>
              <Tabs.TabPane tab="Dưới 100k" key="item-4">
                <ProductTabContent />
              </Tabs.TabPane>
              <Tabs.TabPane tab="Đánh giá cao" key="item-5">
                <ProductTabContent />
              </Tabs.TabPane>
            </Tabs>
              <a href="/" className="view-all">Xem tất cả</a>
          </div>
      </div>
        <div className="product-tab-view_more">
            <Button>Xem thêm</Button>
        </div>
    </div>
  )
}

export default ProductTab