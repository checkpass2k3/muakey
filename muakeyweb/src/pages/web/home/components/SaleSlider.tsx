import { Swiper, SwiperSlide } from "swiper/react";
import {Navigation } from "swiper";
import "../styles/saleslider.scss"
import { Button } from "antd";
import {PlusOutlined,CloseCircleOutlined} from "@ant-design/icons"
// Import Swiper styles
import "swiper/css";
import "swiper/css/navigation";
import steamAvaibale from "../../../../assets/images/steam-avaiable.jpg" 

const SaleSlider = () => {
  return (
    <Swiper
        slidesPerView={1}
        spaceBetween={10}
        modules={[Navigation]}
        navigation={true}
        loop={true}
        breakpoints={{
          640: {
            slidesPerView: 2,
            spaceBetween: 20,
          },
          768: {
            slidesPerView: 4,
            spaceBetween: 40,
          },
          1024: {
            slidesPerView: 5,
            spaceBetween: 50,
          },
        }}
        className="mySwiper sale-slider"
      >
        <SwiperSlide className="sale-slider__item">
          <div className="add-cart">
            <div className="add-cart__hover">
              <img src={steamAvaibale} alt="" />
              <div className="btn-add-cart">
                {/* <Button>
                  <PlusOutlined />
                  <span>Thêm giỏ hàng</span> 
                </Button> */}
                <Button>
                <CloseCircleOutlined />
                  <span>Hết hàng</span> 
                </Button>
              </div>            
              <div className="sold-quantity">
              <span className="incredibly-cheap"></span>
                  <span className="sold">
                  Đã bán 966</span>
              </div>
            </div>
            
            <div className="price-title">
              <a href=""><p className="title">Gói twitch Pubg Gói twitch Pubg Gói twitch Pubg Gói twitch Pubg Gói twitch Pubg</p></a>
              <p className="price">20.000đ</p>
            </div>
          </div>    
          
        </SwiperSlide>
        <SwiperSlide className="sale-slider__item">
          <div className="add-cart">
            <div className="add-cart__hover">
              <img src={steamAvaibale} alt="" />
              <div className="btn-add-cart">
                <Button>
                  <PlusOutlined />
                  <span>Thêm giỏ hàng</span> 
                </Button>
              </div>            
              <div className="sold-quantity">
              <span className="incredibly-cheap"></span>
                  <span className="sold">
                  Đã bán 966</span>
              </div>
            </div>
            
            <div className="price-title">
              <a href=""><p className="title">Gói twitch Pubg Gói twitch Pubg Gói twitch Pubg Gói twitch Pubg Gói twitch Pubg</p></a>
              <p className="price">20.000đ</p>
            </div>
          </div>    
          
        </SwiperSlide>
        <SwiperSlide className="sale-slider__item">
          <div className="add-cart">
            <div className="add-cart__hover">
              <img src={steamAvaibale} alt="" />
              <div className="btn-add-cart">
                <Button>
                  <PlusOutlined />
                  <span>Thêm giỏ hàng</span> 
                </Button>
              </div>            
              <div className="sold-quantity">
              <span className="incredibly-cheap"></span>
                  <span className="sold">
                  Đã bán 966</span>
              </div>
            </div>
            
            <div className="price-title">
              <a href=""><p className="title">Gói twitch Pubg Gói twitch Pubg Gói twitch Pubg Gói twitch Pubg Gói twitch Pubg</p></a>
              <p className="price">20.000đ</p>
            </div>
          </div>    
          
        </SwiperSlide>
        <SwiperSlide className="sale-slider__item">
          <div className="add-cart">
            <div className="add-cart__hover">
              <img src={steamAvaibale} alt="" />
              <div className="btn-add-cart">
                <Button>
                  <PlusOutlined />
                  <span>Thêm giỏ hàng</span> 
                </Button>
              </div>            
              <div className="sold-quantity">
              <span className="incredibly-cheap"></span>
                  <span className="sold">
                  Đã bán 966</span>
              </div>
            </div>
            
            <div className="price-title">
              <a href=""><p className="title">Gói twitch Pubg Gói twitch Pubg Gói twitch Pubg Gói twitch Pubg Gói twitch Pubg</p></a>
              <p className="price">20.000đ</p>
            </div>
          </div>    
          
        </SwiperSlide>
        <SwiperSlide className="sale-slider__item">
          <div className="add-cart">
            <div className="add-cart__hover">
              <img src={steamAvaibale} alt="" />
              <div className="btn-add-cart">
                <Button>
                  <PlusOutlined />
                  <span>Thêm giỏ hàng</span> 
                </Button>
              </div>            
              <div className="sold-quantity">
              <span className="incredibly-cheap"></span>
                  <span className="sold">
                  Đã bán 966</span>
              </div>
            </div>
            
            <div className="price-title">
              <a href=""><p className="title">Gói twitch Pubg Gói twitch Pubg Gói twitch Pubg Gói twitch Pubg Gói twitch Pubg</p></a>
              <p className="price">20.000đ</p>
            </div>
          </div>    
          
        </SwiperSlide>
        <SwiperSlide className="sale-slider__item">
          <div className="add-cart">
            <div className="add-cart__hover">
              <img src={steamAvaibale} alt="" />
              <div className="btn-add-cart">
                <Button>
                  <PlusOutlined />
                  <span>Thêm giỏ hàng</span> 
                </Button>
              </div>            
              <div className="sold-quantity">
              <span className="incredibly-cheap"></span>
                  <span className="sold">
                  Đã bán 966</span>
              </div>
            </div>
            
            <div className="price-title">
              <a href=""><p className="title">Gói twitch Pubg Gói twitch Pubg Gói twitch Pubg Gói twitch Pubg Gói twitch Pubg</p></a>
              <p className="price">20.000đ</p>
            </div>
          </div>    
          
        </SwiperSlide>
        <SwiperSlide className="sale-slider__item">
          <div className="add-cart">
            <div className="add-cart__hover">
              <img src={steamAvaibale} alt="" />
              <div className="btn-add-cart">
                <Button>
                  <PlusOutlined />
                  <span>Thêm giỏ hàng</span> 
                </Button>
              </div>            
              <div className="sold-quantity">
              <span className="incredibly-cheap"></span>
                  <span className="sold">
                  Đã bán 966</span>
              </div>
            </div>
            
            <div className="price-title">
              <a href=""><p className="title">Gói twitch Pubg Gói twitch Pubg Gói twitch Pubg Gói twitch Pubg Gói twitch Pubg</p></a>
              <p className="price">20.000đ</p>
            </div>
          </div>    
          
        </SwiperSlide>
        </Swiper>
  )
}

export default SaleSlider