import { Swiper, SwiperSlide } from "swiper/react";
// Import Swiper styles
import "swiper/css";
import "swiper/css/pagination";
import "../styles/slider.scss"
import { Pagination,Autoplay } from "swiper";
import slider1 from "../../../../assets/images/slider1.png"
import slider2 from "../../../../assets/images/slider2.png"
import slider3 from "../../../../assets/images/slider3.png"

const Slider = () => {
  return (
        <Swiper autoplay={{
            delay: 2500,
            disableOnInteraction: false,
          }} pagination={true} loop={true} modules={[Autoplay,Pagination]} className="mySwiper slider">
            <SwiperSlide className="slider-item" style={{backgroundImage: `url(${slider1})`}}>
                <div className="slider-item__content">
                    <div className="slider-item__content--title">Top up, Nạp game mobile</div>
                    <div className="slider-item__content--access">Truy cập ngay để nạp game</div>
                    <a className="slider-item__content--viewmore">Xem thêm</a> 
                </div>
            </SwiperSlide>
            <SwiperSlide className="slider-item" style={{backgroundImage: `url(${slider2})`}}>
                <div className="slider-item__content">
                    <div className="slider-item__content--title">Top up, Nạp game mobile</div>
                    <div className="slider-item__content--access">Truy cập ngay để nạp game</div>
                    <a className="slider-item__content--viewmore">Xem thêm</a>
                </div>
            </SwiperSlide>
            <SwiperSlide className="slider-item" style={{backgroundImage: `url(${slider3})`}}>
                <div className="slider-item__content">
                    <div className="slider-item__content--title">Top up, Nạp game mobile</div>
                    <div className="slider-item__content--access">Truy cập ngay để nạp game</div>
                    <a className="slider-item__content--viewmore">Xem thêm</a>
                </div>
            </SwiperSlide>
        </Swiper>
  )
}

export default Slider