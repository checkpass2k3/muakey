import SyntheticGame from "./SyntheticGame"
import "../styles/gamemobile.scss"

const GameMobile = () => {
  return (
    <div className="game-mobile-gift-card">
        <div className="game-mobile">
            <SyntheticGame title={"Game Mobile - Top up"} />
        </div>
        <div className="gift-card">
            <SyntheticGame title={"Gift Card"} />
        </div>
    </div>
  )
}

export default GameMobile