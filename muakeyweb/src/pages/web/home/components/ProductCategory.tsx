import { Swiper, SwiperSlide } from "swiper/react";
import "../styles/productcategory.scss"
import React from "react"

// Import Swiper styles
import "swiper/css";
import "swiper/css/navigation";
import { Navigation } from "swiper";
import gameMobile from "../../../../assets/images/game-mobile.png"
import gamePC from "../../../../assets/images/game-pc.png"
import giftCard from "../../../../assets/images/gift-card.png"
import accountUtility from "../../../../assets/images/utility-account.png"
import xbox from "../../../../assets/images/xbox.png"
import primeGaming from "../../../../assets/images/prime-gaming.png"
import accountGameCheap from "../../../../assets/images/cheap-game-account.png"

const ProductCategory = () => {

  return ( 
      <Swiper
        slidesPerView={8}
        watchSlidesProgress={true}
        // loop={true}
        className="mySwiper product-slider"
      >
        <SwiperSlide className="product-slider__item">
          <a href="">
            <img src={gameMobile} alt="" />
            <p>Nạp game mobile</p>
          </a>
        </SwiperSlide>
        <SwiperSlide className="product-slider__item">
            <a href="">
                <img src={gamePC} alt="" />
                <p>Game Máy tính</p>
            </a>
        </SwiperSlide>
        <SwiperSlide className="product-slider__item">
            <a href="">
                <img src={giftCard} alt="" />
                <p>Gift Card</p>
            </a>
        </SwiperSlide>
        <SwiperSlide className="product-slider__item">
            <a href="">
                <img src={accountUtility} alt="" />
                <p>Tài khoản tiện ích</p>
            </a>
            </SwiperSlide>
        <SwiperSlide className="product-slider__item">
            <a href="">
                <img src={xbox} alt="" />
                <p>xbox</p>
            </a>
            </SwiperSlide>
        <SwiperSlide className="product-slider__item">
            <a href="">
                <img src={primeGaming} alt="" />
                <p>Prime Gaming</p>
            </a>
        </SwiperSlide>
        <SwiperSlide className="product-slider__item">
            <a href="">
                <img src={accountGameCheap} alt="" />
                <p>Tài khoản game giá rẻ</p>
            </a>
        </SwiperSlide>
        <SwiperSlide className="product-slider__item">
          <a href="">
            <img src={gameMobile} alt="" />
            <p>Nạp game mobile</p>
          </a>
        </SwiperSlide>
        <SwiperSlide className="product-slider__item">
            <a href="">
                <img src={gamePC} alt="" />
                <p>Game Máy tính</p>
            </a>
        </SwiperSlide>
        <SwiperSlide className="product-slider__item">
            <a href="">
                <img src={giftCard} alt="" />
                <p>Gift Card</p>
            </a>
        </SwiperSlide>
      </Swiper>
  )
}

export default ProductCategory