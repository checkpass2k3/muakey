import "../styles/homecategoryone.scss"
import GamePC from "./GamePC"
import SyntheticGame from "./SyntheticGame"


const HomeCategoryOne = () => {
  return (
    <div className="home-category-one">
        <div className="game-pc">
            <SyntheticGame title={"Game PC"} />
        </div>
        <GamePC title={"PUBG PC"} />
    </div>
  )
}

export default HomeCategoryOne