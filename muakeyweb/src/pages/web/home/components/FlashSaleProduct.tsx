import SaleSlider from "./SaleSlider"
import "../styles/flashsale.scss"

const FlashSaleProduct = () => {
  return (
    <div className="flash-sale">
        <div className="all-cate__head--title header-container flash-sale__title">Sản phẩm bán chạy</div>
        <SaleSlider />
    </div>
    
  )
}

export default FlashSaleProduct