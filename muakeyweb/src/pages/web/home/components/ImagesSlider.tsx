import { Swiper, SwiperSlide } from "swiper/react";
import "../styles/imagesslider.scss"
import skin from "../../../../assets/images/skin-pubg.jpg"
import minecraft from "../../../../assets/images/minecraft-version.jpg"
import gameBlizzard from "../../../../assets/images/game-blizzard.jpg"

const ImagesSlider = () => {
  return (
        <Swiper
        slidesPerView={1}
        spaceBetween={10}
       
        breakpoints={{
          640: {
            slidesPerView: 2,
            spaceBetween: 20,
          },
          768: {
            slidesPerView: 4,
            spaceBetween: 40,
          },
          1024: {
            slidesPerView: 5,
            spaceBetween: 50,
          },
        }}
        className="mySwiper images-slider"
      >
        <SwiperSlide className="images-slider__item">
          <a href="">
            <img src={skin} alt="" />
          </a>
        </SwiperSlide>
        <SwiperSlide className="images-slider__item">
          <a href="">
            <img src={minecraft} alt="" />
          </a>
        </SwiperSlide>
        <SwiperSlide className="images-slider__item">
          <a href="">
            <img src={gameBlizzard} alt="" />
          </a>
        </SwiperSlide>

        
      </Swiper>
    
  )
}

export default ImagesSlider