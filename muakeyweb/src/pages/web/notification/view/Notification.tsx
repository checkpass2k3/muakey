import "../styles/notification.scss"
import noti from  "../../../../assets/images/notification-empty.svg"
import {Tabs} from "antd"

const Notification = () => {
  return (
    <div className="notification center">
      <div className="notification-content">
        <h1>Thông báo</h1>
        <div className="btn">
          <Tabs type='card'>
              <Tabs.TabPane tab="Tất cả" key="item-1">
              </Tabs.TabPane>
              <Tabs.TabPane tab="Chưa đọc" key="item-2">
              </Tabs.TabPane>
            </Tabs>
        </div>
        <div className="center">
          <div>
            <div className="center">
            <img src={noti} alt="" />

            </div>
            <p style={{fontWeight: "550",fontSize:"16px"}}>Chưa có thông báo nào!</p>
          </div>
        </div>
      </div>
    </div>
  )
}

export default Notification