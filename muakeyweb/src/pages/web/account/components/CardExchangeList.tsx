import viettel from "../../../../assets/images/viettel-payment.svg"
import mobiphone from "../../../../assets/images/mobiphone.png"
import vinaphone from "../../../../assets/images/vinaphone.svg"
import vietnammobile from "../../../../assets/images/vietnammobile.svg"
import garena from "../../../../assets/images/garena.svg"
import zing from "../../../../assets/images/zing.svg"
import { Col, Row } from "antd"
import "../styles/card-exchange-list.scss"

const CardExchangeList = () => {
  return (
    <Row className="card-exchange-list">
        <Col className="card-exchange-list-item center" md={4} xs={8} lg={4}>
            <div>
                <div className="card-image center">
                    <img src={viettel} alt="" />
                </div>
                <div className="center">Viettel</div>
                <span className="center">CK 32%</span>
            </div>
        </Col>
        <Col className="card-exchange-list-item center" md={4} xs={8} lg={4}>
            <div>
                <div className="card-image center">
                    <img src={mobiphone} alt="" />
                </div>
                <div className="center">Mobiphone</div>
                <span className="center">CK 32%</span>
            </div>
        </Col>
        <Col className="card-exchange-list-item center" md={3} xs={8} lg={4}>
            <div>
                <div className="card-image center">
                    <img src={vinaphone} alt="" />
                </div>
                <div className="center">Vinaphone</div>
                <span className="center">CK 32%</span>
            </div>
        </Col>
        <Col className="card-exchange-list-item center" md={4} xs={8} lg={4}>
            <div>
                <div className="card-image center">
                    <img src={vietnammobile} alt="" />
                </div>
                <div className="center">Vietnammobile</div>
                <span className="center">CK 32%</span>
            </div>
        </Col>
        <Col className="card-exchange-list-item center" md={4} xs={8} lg={4}>
            <div>
                <div className="card-image center">
                    <img src={garena} alt="" />
                </div>
                <div className="center">Garena</div>
                <span className="center">CK 32%</span>
            </div>
        </Col>
        <Col className="card-exchange-list-item center" md={4} xs={8} lg={4}>
            <div>
                <div className="card-image center">
                    <img src={zing} alt="" />
                </div>
                <div className="center">Zing</div>
                <span className="center">CK 32%</span>
            </div>
        </Col>
    </Row>
  )
}

export default CardExchangeList