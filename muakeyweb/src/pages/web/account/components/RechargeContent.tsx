import vietcombankPay from "../../../../assets/images/recharge/vietcombank.svg"
import momo from "../../../../assets/images/recharge/momo.svg"
import viettel from "../../../../assets/images/recharge/viet.svg"
import mobiphone from "../../../../assets/images/mobiphone.png"
import vinaphone from "../../../../assets/images/vinaphone.svg"
import vietnammobile from "../../../../assets/images/vietnammobile.svg"
import garena from "../../../../assets/images/garena.svg"
import zing from "../../../../assets/images/zing.svg"

const RechargeContent = () => {
  return (
    <div className='recharge-content'>
        <a href="/recharge/bank-transfer" className="recharge-content-item">
            <div className="center recharge-content-item-image">
                <img src={vietcombankPay} alt="" />
            </div>
            <div className="recharge-content-item-name">
                <p>Tài khoản ngân hàng</p>
                <span>Tài khoản ngân hàng</span>
            </div>
        </a>
        <a href="" className="recharge-content-item">
            <div className="center recharge-content-item-image">
                <img src={momo} alt="" />
            </div>
            <div className="recharge-content-item-name">
                <p>Tài khoản ngân hàng</p>
                <span>Tài khoản ngân hàng</span>
            </div>
        </a>
        <a href="" className="recharge-content-item">
            <div className="center recharge-content-item-image">
                <img src={viettel} alt="" />
            </div>
            <div className="recharge-content-item-name">
                <p>Tài khoản ngân hàng</p>
                <span>Tài khoản ngân hàng</span>
            </div>
        </a>
    </div>
  )
}

export default RechargeContent