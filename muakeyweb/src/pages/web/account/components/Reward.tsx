import coin from "../../../../assets/images/coin.png"
import poin from "../../../../assets/images/reward-point.svg"
import "../styles/reward.scss"

const Reward = () => {
  return (
    <div className="account-profile-middle display-flex">
            <div className="surplus">
                <p>Số dư hiện tại</p>
                <div className="flex-justify-between">
                    <div className="center">
                        <div className="center">
                            <img src={coin} alt="" />
                        </div>
                        <span className="money">0đ</span>
                    </div>
                    <a href="">Nạp tiền</a>
                </div>
            </div>
            <div className="reward-points">
                <p>Điểm thưởng</p>
                <div className="flex-justify-between">
                    <div className="center">
                        <div className="center">
                            <img src={poin} alt="" />
                        </div>
                        <span className="point">0</span>
                    </div>
                    <a href="">Đổi điểm</a>
                </div>
            </div>
        </div>
  )
}

export default Reward