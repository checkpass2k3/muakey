import { Table,Select,Input,DatePicker } from 'antd';
import type { ColumnsType } from 'antd/es/table';

interface DataType {
    key: React.Key;
    bank: string;
    money: string;
    status: string;
    time: string;
  }
  
  const columns: ColumnsType<DataType> = [
    {
      title: 'Ngân hàng',
      dataIndex: 'bank',
    },
    {
      title: 'Số tiền',
      dataIndex: 'money',
    },
    {
      title: 'Trạng thái',
      dataIndex: 'status',
    },
    {
        title: 'Thời gian',
        dataIndex: 'time',
      },
  ];

  const data: DataType[] = [];
    for (let i = 0; i < 1; i++) {
    data.push({
        key: i,
        bank:"Vietcombank",
        money: `700.000đ`,
        status: "success",
        time: `31/01/2023 09:27:01`,
    });
}

const HistoryTable = () => {
  return (
    <>
        <div className='display-flex table-head'>
            <Select
                defaultValue="lucy"
                style={{ width: 250 }}
                options={[
                    {
                    value: 'jack',
                    label: 'Jack',
                    },
                    {
                    value: 'lucy',
                    label: 'Lucy',
                    },
                    {
                    value: 'Yiminghe',
                    label: 'yiminghe',
                    },
                ]}
                />
                <div className="table-sort-date">
                  <DatePicker placeholder='Ngày'/>
                </div>
            
        </div>
        <Table columns={columns} dataSource={data} />
    </>
  )
}

export default HistoryTable