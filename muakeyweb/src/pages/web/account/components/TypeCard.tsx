import { Select,Input } from 'antd'
import "../styles/type-card.scss"
import { DeleteFilled } from '@ant-design/icons'

const TypeCard = () => {
  return (
    <div className='type-card'>
        <div className='type-card-list'>
            <div className='type-card-list-item'>
                <p>Loại thẻ*</p>
                <Select
                    defaultValue="lucy"
                    options={[
                        {
                        value: 'jack',
                        label: 'Jack',
                        },
                        {
                        value: 'lucy',
                        label: 'Lucy',
                        },
                    ]}
                    />
            </div>
            <div className='type-card-list-item'>
                <p>Mệnh giá*</p>
                <Select
                    defaultValue="lucy"
                    options={[
                        {
                        value: 'jack',
                        label: 'Jack',
                        },
                        {
                        value: 'lucy',
                        label: 'Lucy',
                        },
                    ]}
                    />
            </div>
            <div className='type-card-list-item'>
                <p>Số seri thẻ*</p>
                <Input placeholder="Nhập số seri thẻ" />
            </div>
            <div className='type-card-list-item'>
                <p>Mã thẻ*</p>
                <Input placeholder="Nhập mã thẻ" />
            </div>
            <div className='' style={{display:"block"}}>
                    <p style={{visibility: "hidden"}}>a</p>
                    <DeleteFilled />
            </div>
        </div>
        <div className='type-card-list'>
            <div className='type-card-list-item'>
                <p style={{display: "none"}}>Loại thẻ*</p>
                <Select
                    defaultValue="lucy"
                    options={[
                        {
                        value: 'jack',
                        label: 'Jack',
                        },
                        {
                        value: 'lucy',
                        label: 'Lucy',
                        },
                    ]}
                    />
            </div>
            <div className='type-card-list-item'>
                <p style={{display: "none"}}>Mệnh giá*</p>
                <Select
                    defaultValue="lucy"
                    options={[
                        {
                        value: 'jack',
                        label: 'Jack',
                        },
                        {
                        value: 'lucy',
                        label: 'Lucy',
                        },
                    ]}
                    />
            </div>
            <div className='type-card-list-item'>
                <p style={{display: "none"}}>Số seri thẻ*</p>
                <Input placeholder="Nhập số seri thẻ" />
            </div>
            <div className='type-card-list-item'>
                <p style={{display: "none"}}>Mã thẻ*</p>
                <Input placeholder="Nhập mã thẻ" />
            </div>
            <div className='' style={{display:"block"}}>
                    <p style={{display: "none"}}>a</p>
                    <DeleteFilled />
            </div>
        </div>
        <div className="add-card">
            <button>Thêm thẻ</button>
        </div>
        <div className="recharge center">
            <button>Nạp tiền</button>
        </div>
    </div>
  )
}

export default TypeCard