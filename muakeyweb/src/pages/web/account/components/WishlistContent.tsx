import { Pagination } from "antd"
import arknights from "../../../../assets/images/arknights.jpg"

const WishlistContent = () => {
  return (
    <div className='wishlists-content'>
        <div className="wishlists-content-item">
            <div className="flex-justify-between">
                <div className="flex-justify-between">
                    <div className="wishlist-product-image">
                        <img src={arknights} alt="" />
                    </div>
                    <div className="wishlist-product-name center">
                        <div className="">
                            <p>40 Kim Cương - 30$ Arknights</p>
                            <span>Order</span>
                        </div>
                    </div>
                </div>
                <div className="center money">585.000đ</div>
            </div>
        </div>
        <div className="wishlists-content-item">
            <div className="flex-justify-between">
                <div className="flex-justify-between">
                    <div className="wishlist-product-image">
                        <img src={arknights} alt="" />
                    </div>
                    <div className="wishlist-product-name center">
                        <div className="">
                            <p>40 Kim Cương - 30$ Arknights</p>
                            <span>Order</span>
                        </div>
                    </div>
                </div>
                <div className="center money">585.000đ</div>
            </div>
        </div>
        <div className="wishlists-content-item">
            <div className="flex-justify-between">
                <div className="flex-justify-between">
                    <div className="wishlist-product-image">
                        <img src={arknights} alt="" />
                    </div>
                    <div className="wishlist-product-name center">
                        <div className="">
                            <p>40 Kim Cương - 30$ Arknights</p>
                            <span>Order</span>
                        </div>
                    </div>
                </div>
                <div className="center money">585.000đ</div>
            </div>
        </div>
        <div className="wishlists-content-item">
            <div className="flex-justify-between">
                <div className="flex-justify-between">
                    <div className="wishlist-product-image">
                        <img src={arknights} alt="" />
                    </div>
                    <div className="wishlist-product-name center">
                        <div className="">
                            <p>40 Kim Cương - 30$ Arknights</p>
                            <span>Order</span>
                        </div>
                    </div>
                </div>
                <div className="center money">585.000đ</div>
            </div>
        </div>
        <div className="wishlists-content-item">
            <div className="flex-justify-between">
                <div className="flex-justify-between">
                    <div className="wishlist-product-image">
                        <img src={arknights} alt="" />
                    </div>
                    <div className="wishlist-product-name center">
                        <div className="">
                            <p>40 Kim Cương - 30$ Arknights</p>
                            <span>Order</span>
                        </div>
                    </div>
                </div>
                <div className="center money">585.000đ</div>
            </div>
        </div>
        <Pagination />
    </div>
  )
}

export default WishlistContent