import React from 'react'
import { Input,Button } from 'antd'
import "../styles/sortwarehouse.scss"
import { SearchOutlined } from '@ant-design/icons'
import {DatePicker} from 'antd'
 
const SortWareHouse = () => {
  return (
    <div className='sort-ware-house'>
        <div className='search'>
          <Input.Group compact className='ware-house-search'>
              <Input style={{ width: '75%' }} placeholder="Tên sản phẩm/Mã đơn hàng..." />
              <Button type="primary"><SearchOutlined className="icon-search" /></Button>
          </Input.Group>
        </div>
        <div className='sort-space'></div>
        <div className='sort-date'>
          <DatePicker placeholder='Ngày' />
        </div>
    </div>
  )
}

export default SortWareHouse