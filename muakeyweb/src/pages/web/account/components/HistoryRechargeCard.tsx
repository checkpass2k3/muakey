import { Table,Select,Input,DatePicker } from 'antd';
import type { ColumnsType } from 'antd/es/table';
import "../styles/history-recharge-card.scss"

interface DataType {
    key: React.Key;
    network: string;
    status: string;
    serial: string;
    codeCard: string;
    valueSent: string;
    realFaceValue: string;
    discount: string;
    recipients: string;
    time: string;
  }
  
  const columns: ColumnsType<DataType> = [
    {
      title: 'Nhà mạng',
      dataIndex: 'network',
    },
    {
      title: 'Trạng thái',
      dataIndex: 'status',
    },
    {
      title: 'Serial',
      dataIndex: 'serial',
    },
    {
      title: 'Mã thẻ',
      dataIndex: 'codeCard',
    },
    {
      title: 'Mệnh giá gửi',
      dataIndex: 'valueSent',
    },
    {
      title: 'Mệnh giá thực',
      dataIndex: 'realFaceValue',
    },
    {
      title: 'Chiết khấu',
      dataIndex: 'discount',
    },
    {
      title: 'Khách nhận',
      dataIndex: 'recipients',
    },
    {
        title: 'Thời gian',
        dataIndex: 'time',
      },
  ];

  const data: DataType[] = [];
    for (let i = 0; i < 1; i++) {
    data.push({
        key: i,
        network:"Vietcombank",
        status: "success",
        serial: "5656355",
        codeCard: "ádlqk4wqe",
        valueSent: "100.000đ",
        realFaceValue: "80.000đ",
        discount: "10%",
        recipients: "80.000 xu",
        time: `31/01/2023 09:27:01`,
    });
}

const HistoryRechargeCard = () => {
  return (
    <div className='history-card'>
      <h1>Lịch sử nạp tiền</h1>
        <div className='display-flex sort-table'>
            <div className="sort-table-item">
              <Select
                defaultValue="Tất cả mạng"
                options={[
                    {
                    value: 'jack',
                    label: 'Jack',
                    },
                    {
                    value: 'lucy',
                    label: 'Lucy',
                    },
                ]}
                />
            </div>
            <div className="sort-table-item">
              <Select
                defaultValue="Trạng thái"
                options={[
                    {
                    value: 'jack',
                    label: 'Jack',
                    },
                    {
                    value: 'lucy',
                    label: 'Lucy',
                    },
                ]}
                />
            </div>
            <div className="sort-table-item">
            <Input placeholder="Mã thẻ/Seri" />
            </div>
            <div className="sort-table-item">
            <DatePicker placeholder='Ngày'/>
            </div>
        </div>
        <Table columns={columns} dataSource={data} />
    </div>
  )
}

export default HistoryRechargeCard