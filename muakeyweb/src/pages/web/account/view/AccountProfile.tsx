import "../styles/accountprofile.scss"
import goldRank from "../../../../assets/images/gold-rank.svg"
import avatar from "../../../../assets/images/avatar.jpg"
import {MailFilled,CheckCircleFilled,EditFilled} from "@ant-design/icons"
import Reward from "../components/Reward"

const AccountProfile = () => {
  return (
    <div className='account-profile'>
        <div className="account-profile-top">
            <div className="nick flex-justify-between">
                <div className="nick-left">
                    <div className="avatar">
                        <img src={avatar} alt="" />
                    </div>
                    <div className="center">
                        <img src={goldRank} alt="" />
                        <span>Thành viên ROOKIE</span>
                    </div>
                </div>
                <div className="nick-right">
                    <a className="link" href="/account/profile/edit">Cập nhật tài khoản</a>
                    {/* <a className="icon-edit" href=""><EditFilled /></a> */}
                </div>
            </div>
            <div className="account-profile-top__bot">
                <MailFilled />
                <span>satthan0107@gmail.com</span>
                <CheckCircleFilled style={{color: "green"}} />
            </div>
        </div>
        <Reward />
        <div className="ware-house">
            <div className="flex-justify-between ware-house-top">
                <h1>Kho hàng</h1>
                <div>
                    <a href="">Xem tất cả</a>
                </div>
            </div>
            <div className="ware-house-bot display-flex-space-around">
                <div className="ware-house-bot__item">
                    <div className="text-center">0</div>
                    <p>Chờ xử lý</p>
                </div>
                <div className="ware-house-bot__item">
                    <div className="text-center">0</div>
                    <p>Đang xử lý</p>
                </div>
                <div className="ware-house-bot__item">
                    <div className="text-center">0</div>
                    <p>Hoàn thành</p>
                </div>
                <div className="ware-house-bot__item">
                    <div className="text-center">0</div>
                    <p>Bị hủy</p>
                </div>
            </div>
        </div>
    </div>
  )
}

export default AccountProfile