import "../styles/transactionhistories.scss"
import { Table } from 'antd';
import type { ColumnsType } from 'antd/es/table';

interface DataType {
    key: React.Key;
    time: string;
    money: string;
    surplus: string;
    content: string;
  }
  
  const columns: ColumnsType<DataType> = [

    {
      title: 'Thời gian',
      dataIndex: 'time',
    },
    {
        title: 'Số tiền',
        dataIndex: 'money',
      },
    {
      title: 'Số dư',
      dataIndex: 'surplus',
    },
    {
        title: 'Nội dung',
        dataIndex: 'content',
      },

  ];

  const data: DataType[] = [];
    for (let i = 0; i < 46; i++) {
    data.push({
        key: i,
        time: `31/01/2023 09:27:01`,
        money: "string",
        surplus: "string",
        content: "string",
    });
}


const TransactionHistories = () => {
  return (
    <div className="transaction-histories">
        <h1>Lịch sử giao dịch</h1>
        <div className="transaction-histories-table">
            <Table columns={columns} dataSource={data} />
        </div>
    </div>
  )
}

export default TransactionHistories