import "../styles/card-exchange.scss"
import CardExchangeList from "../components/CardExchangeList"
import { AlertOutlined } from "@ant-design/icons"
import TypeCard from "../components/TypeCard"
import HistoryRechargeCard from "../components/HistoryRechargeCard"

const CardExchange = () => {
  return (
    <div className="card-exchange">
        <h1>Nạp tiền qua thẻ cào</h1>
        <CardExchangeList />
        <div className="card-exchange-alert">
          <AlertOutlined />
            <span>
  Cảnh báo nguy hiểm: Quý khách điền sai mệnh giá - mã - seri thẻ sẽ tự chịu trách nhiệm khi bị mất thẻ!</span>
        </div>
        <TypeCard />
        <HistoryRechargeCard />
    </div>
  )
}

export default CardExchange