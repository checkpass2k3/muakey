import "../styles/profile-edit.scss"
import {  EditFilled } from '@ant-design/icons';
import {
    Button,
    Form,
    Select,
    Upload,
    Input,
    Row,
    Col,
    DatePicker
  } from 'antd';

const { Option } = Select;


const normFile = (e: any) => {
  console.log('Upload event:', e);
  if (Array.isArray(e)) {
    return e;
  }
  return e?.fileList;
};

const ProfileEdit = () => {
  return (
    <div className='profile-edit'>
        <h1>Cập nhật thông tin các nhân</h1>
        <div className="profile-edit-form">
        <Form
            name="validate_other"
            initialValues={{
                'input-number': 3,
                'checkbox-group': ['A', 'B'],
                rate: 3.5,
            }}
        >
        <Row className="profile">
            <Col md={24} lg={24} xs={24}>
                <Form.Item
                        name="upload"
                        valuePropName="fileList"
                        getValueFromEvent={normFile}
                        className="center"
                    >
                <Upload name="logo" action="/upload.do" listType="picture">
                <Button icon={<EditFilled />}></Button>
                </Upload>
            </Form.Item>
            </Col>
            <Col md={12} lg={12} xs={12} className="profile-item">
                <Form.Item messageVariables={{ another: 'good' }}>
                    <span className="title">User name*</span>
                    <Input disabled placeholder="minato0107" />
                </Form.Item>
            </Col>
            
            <Col md={12} lg={12} xs={12} className="profile-item">
                
                <Form.Item messageVariables={{ another: 'good' }}>
                    <span className="title">Họ và tên</span>
                    <Input />
                </Form.Item>
            </Col>

            <Col md={12} lg={12} xs={12} className="profile-item">
                <Form.Item messageVariables={{ another: 'good' }}>
                <span className="title">Email*</span>
                    <Input type="email" value="abc@gmai.com" />
                </Form.Item>
            </Col>
            
            <Col md={12} lg={12} xs={12} className="profile-item">
                <Form.Item messageVariables={{ another: 'good' }}>
                    <span className="title">Số điện thoại</span>
                    <Input />
                </Form.Item>
            </Col>
            <Col md={12} lg={12} xs={12} className="profile-item">
                <Form.Item>
                <p className="title">Ngày sinh</p>
                    <DatePicker placeholder="Chọn ngày" />
                </Form.Item>
            </Col>
            <Col md={12} lg={12} xs={12} className="profile-item">
                <Form.Item messageVariables={{ another: 'good' }}>
                    <span className="title">Địa chỉ</span>
                    <Input />
                </Form.Item>
            </Col>
            <Col>
        <Form.Item wrapperCol={{ offset: 8, span: 16 }}>
                <Button type="primary" htmlType="submit">
                    Cập nhật
                </Button>
                </Form.Item>
      </Col>
      </Row>
      
            </Form>
        
        </div>
    </div>
  )
}

export default ProfileEdit