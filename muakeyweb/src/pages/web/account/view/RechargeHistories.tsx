import "../styles/rechargehistories.scss"
import { Table } from 'antd';
import type { ColumnsType } from 'antd/es/table';

interface DataType {
    key: React.Key;
    time: string;
    status: string;
    method: string;
    money: string;
    discount: string;
    actuallyReceived: string;
  }
  
  const columns: ColumnsType<DataType> = [

    {
      title: 'Thời gian',
      dataIndex: 'time',
    },
    {
        title: 'Trạng thái',
        dataIndex: 'status',
      },
    {
      title: 'Phương thức',
      dataIndex: 'method',
    },
    {
        title: 'Số tiền',
        dataIndex: 'money',
      },
    {
        title: 'Chiết khấu(%)',
        dataIndex: 'discount',
      },
      {
        title: 'Thực nhận',
        dataIndex: 'actuallyReceived',
      },
  ];

  const data: DataType[] = [];
    for (let i = 0; i < 46; i++) {
    data.push({
        key: i,
        time: `31/01/2023 09:27:01`,
        status: `31/01/2023 09:27:01`,
        method: "Chuyển khoản",
        money: "700.000đ",
        discount: `Việt Nam`,
        actuallyReceived: `Việt Nam`,
    });
}

const RechargeHistories = () => {
  return (
    <div className='recharge-histories'>
        <h1>Lịch sử nạp tiền</h1>
        <div className="recharge-histories-table">
        <Table columns={columns} dataSource={data} />
        </div>
    </div>
  )
}

export default RechargeHistories