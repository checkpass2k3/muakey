import "../styles/loginhis.scss"
import { Table } from 'antd';
import type { ColumnsType } from 'antd/es/table';
import React, { useState } from 'react';

interface DataType {
    key: React.Key;
    ip: string;
    time: string;
    deviced: string;
    national: string;
  }
  
  const columns: ColumnsType<DataType> = [
    {
      title: 'Ip',
      dataIndex: 'ip',
    },
    {
      title: 'Thời gian',
      dataIndex: 'time',
    },
    {
      title: 'Thiết bị',
      dataIndex: 'deviced',
    },
    {
        title: 'Quốc gia',
        dataIndex: 'national',
      },
  ];

  const data: DataType[] = [];
    for (let i = 0; i < 46; i++) {
    data.push({
        key: i,
        ip:"42.114.253.86",
        time: `31/01/2023 09:27:01`,
        deviced: "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/109.0.0.0 Safari/537.36",
        national: `Việt Nam`,
    });
}


const LoginHistories = () => {

  return (
    <div className='login-histories'>
        <h1>Lịch sử đăng nhập</h1>
        <div className="login-histories-table">
            <Table columns={columns} dataSource={data} />
        </div>
    </div>
  )
}

export default LoginHistories