import React from 'react'
import "../styles/warehouse.scss"
import { Tabs } from 'antd';
import SortWareHouse from '../components/SortWareHouse';

const WareHouse = () => {
  return (
    <div className='warehouse'>
      <h1>Kho hàng</h1>
      <div className="warehouse-tab">
        <Tabs defaultActiveKey="1">
          <Tabs.TabPane tab="Tất cả (0)" key="1">
            <SortWareHouse />
            Content of Tab Pane 1
          </Tabs.TabPane>
          <Tabs.TabPane tab="Chờ xử lý (0)" key="2">
          <SortWareHouse />
            Content of Tab Pane 2
          </Tabs.TabPane>
          <Tabs.TabPane tab="Đang xử lý (0)" key="3">
          <SortWareHouse />
            Content of Tab Pane 3
          </Tabs.TabPane>
          <Tabs.TabPane tab="Bị hủy (0)" key="4">
          <SortWareHouse />
            Content of Tab Pane 4
          </Tabs.TabPane>
          <Tabs.TabPane tab="Hoàn thành (0)" key="5">
          <SortWareHouse />
            Content of Tab Pane 5
          </Tabs.TabPane>
        </Tabs>
      </div>
    </div>
  )
}

export default WareHouse