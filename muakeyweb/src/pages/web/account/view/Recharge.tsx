import RechargeContent from "../components/RechargeContent"
import "../styles/recharge.scss"

const Recharge = () => {
  return (
    <div className="recharge">
        <h1>Nạp tiền</h1>
        <RechargeContent />
    </div>
  )
}

export default Recharge