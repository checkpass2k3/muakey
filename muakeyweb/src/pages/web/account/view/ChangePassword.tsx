import "../styles/changepass.scss"
import { Form,Button,Input } from 'antd'
import { FaArrowRight,FaUserCircle,FaLock,FaCheckCircle } from 'react-icons/fa'

const ChangePassword = () => {
  return (
    <div className="change-pass">
        <h1>Đổi mật khẩu</h1>
        <div className="content-form">
            <Form
            name="basic"
            labelCol={{ span: 8 }}
            wrapperCol={{ span: 16 }}
            autoComplete="off"
            >
                <Form.Item
                    className='form-password position-rel '
                    label="Mật khẩu hiện tại"
                    rules={[{ required: true, message: 'Please input your password!' }]}
                >
                    <Input.Password className='password-input padding-0 border-rd-4 background-none'/>
                    
                </Form.Item>
                <Form.Item
                    className='form-password position-rel '
                    label="Mật khẩu mới"
                    rules={[{ required: true, message: 'Please input your password!' }]}
                >
                    <Input.Password className='password-input padding-0 border-rd-4 background-none'/>
                    <div className="validate">
                        <p className="validate-item validate-error">
                            <FaCheckCircle />
                            <span>Gồm 6 ~ 12 ký tự</span>
                        </p>
                        <p className="validate-item validate-success">
                            <FaCheckCircle />
                            <span>Gồm ký tự in hoa, in thường và số</span>
                        </p>
                        <p className="validate-item">
                            <FaCheckCircle />
                            <span>Gồm ký tự đặc biệt</span>
                        </p>
                    </div>
                    
                
                </Form.Item>
                <Form.Item
                    className='form-password position-rel '
                    label="Nhập lại mật khẩu"
                    rules={[{ required: true, message: 'Please input your password!' }]}
                >
                    <Input.Password className='password-input padding-0 border-rd-4 background-none'/>
                
                </Form.Item>
                <Form.Item wrapperCol={{ offset: 8, span: 24 }}>
                    <Button type="primary" htmlType="submit">
                        Đổi mật khẩu
                    </Button>
                </Form.Item>
            </Form>
        </div>
    </div>
  )
}

export default ChangePassword