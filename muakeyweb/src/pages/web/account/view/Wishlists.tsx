import WishlistContent from "../components/WishlistContent"
import "../styles/wishlists.scss"

const Wishlists = () => {
  return (
    <div className="wishlists">
        <h1>Sản phẩm yêu thích</h1>
        <WishlistContent />
    </div>
  )
}

export default Wishlists