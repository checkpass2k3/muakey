import "../styles/banktransfer.scss"
import { AlertOutlined } from "@ant-design/icons"
import { InputNumber,Checkbox } from "antd"
import {useState} from "react"
import HistoryTable from "../components/HistoryTable"


const BankTransfer = () => {
    
    const [checkNote,setCheckNote] = useState<boolean>(false)

  return (
    <div className="bank-transfer">
        <h1>Nạp tiền qua tài khoản ngân hàng</h1>
        <div className="bank-transfer-content">
            <div className="bank-transfer-content-center">
                <div className="alert">
                    <div className="center">
                        <div className="display-flex">
                            <AlertOutlined />
                            <div>
        Nên chuyển cùng ngân hàng (Vietcombank) để nhận được tiền nhanh nhất. Nếu chuyển khác ngân hàng bạn chọn hình thức chuyển tiền nhanh 24/7. Chi tiết chuyển khoản sẽ được hướng dẫn sau khi bạn xác nhận số tiền.</div>
                        </div>
                        
                    </div>
                </div>
                <div className="input-money">
                <InputNumber addonAfter="VNĐ" defaultValue={100} />
                </div>
                <div className="input-check">
                    <Checkbox onChange={() => setCheckNote(!checkNote)}>Tôi hiểu rằng, khi chuyển khoản phải chính xác hoàn toàn nội dung chuyển khoản và số tiền.</Checkbox>
                </div>
                <button className={!checkNote ? "no-active":""}>Nạp tiền</button>
            </div>
        </div>
        <div className="bank-history">
            <h1>Lịch sử nạp tiền</h1>

            <div className="bank-table">
                <HistoryTable />
            </div>
        </div>
    </div>
  )
}

export default BankTransfer