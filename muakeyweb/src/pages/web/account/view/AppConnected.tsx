import "../styles/appconnected.scss"
import { FaGoogle,FaFacebook,FaSteam,FaDiscord,FaTiktok } from "react-icons/fa"

const AppConnected = () => {
  return (
    <div className="app-connected">
        <div className="app-connected-item flex-justify-between">
            <div className="center">
                <FaGoogle style={{fontSize: "36px"}} />
                <span>Google</span>
            </div>
            <button>Liên kết</button>
        </div>
        <div className="app-connected-item flex-justify-between">
            <div className="center">
                <FaFacebook style={{fontSize: "36px"}} />
                <span>Facebook</span>
            </div>
            <button>Liên kết</button>
        </div>
        <div className="app-connected-item flex-justify-between">
            <div className="center">
                <FaSteam style={{fontSize: "36px"}} />
                <span>Steam</span>
            </div>
            <button>Liên kết</button>
        </div>
        <div className="app-connected-item flex-justify-between">
            <div className="center">
                <FaDiscord style={{fontSize: "36px"}} />
                <span>Discord</span>
            </div>
            <button>Liên kết</button>
        </div>
        <div className="app-connected-item flex-justify-between">
            <div className="center">
                <FaTiktok style={{fontSize: "36px"}} />
                <span>Tiktok</span>
            </div>
            <button>Liên kết</button>
        </div>
    </div>
  )
}

export default AppConnected