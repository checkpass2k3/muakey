import '../styles/account.scss'
import personal from "../../../../assets/images/personal.svg"
import key2 from "../../../../assets/images/key2.svg"
import currency from "../../../../assets/images/currency-dollar.svg"
import voucher from "../../../../assets/images/voucher.svg"
import warehouse from "../../../../assets/images/warehouse.svg"
import recharge from "../../../../assets/images/recharge-history.svg"
import transaction from "../../../../assets/images/transaction-history.svg"
import favorite from "../../../../assets/images/favorite.svg"
import {
  MenuFoldOutlined,
  MenuUnfoldOutlined,
} from '@ant-design/icons'
import { useState } from "react"
import { Outlet, useNavigate } from 'react-router-dom'
import { Menu, Button } from 'antd'
import type { MenuProps } from 'antd';

type MenuItem = Required<MenuProps>['items'][number];

function getItem(
  label: React.ReactNode,
  key: React.Key,
  icon?: React.ReactNode,
  children?: MenuItem[],
  type?: 'group',
): MenuItem {
  return {
    key,
    icon,
    children,
    label,
    type,
  } as MenuItem;
}

const items: MenuItem[] = [
  getItem('Tài khoản của tôi', '', <img src={personal} alt="" />, [
    getItem('Thông tin cá nhân', '/web/account/profile'),
    getItem('Đổi mật khẩu', '/web/account/change-passowrd'),
    getItem('Lịch sử đăng nhập', '/web/account/history'),
    getItem('Ứng dụng được kết nối', '/web/account/connect'),
  ]),
  getItem('Nạp tiền', '1', <img src={currency} alt="" />),
  getItem('Đổi điểm', '2', <img src={key2} alt="" />),
  getItem('Kho voucher', '3', <img src={voucher} alt="" />),
  getItem('Kho hàng', '9', <img src={warehouse} alt="" />),
  getItem('Lịch sử nạp tiền', '10', <img src={recharge} alt="" />),
  getItem('Lịch sử giao dịch', '11', <img src={transaction} alt="" />),
  getItem('Sản phẩm yêu thích', '12', <img src={favorite} alt="" />),
];

const Account = () => {
  const navigate = useNavigate();
  const [collapsed, setCollapsed] = useState(false);

  const toggleCollapsed = () => {
    setCollapsed(!collapsed);
  };

  const onClick: MenuProps['onClick'] = e => {
    navigate(e.key);
  };

  return (
    <div className='account'>
      <div className='header-container display-flex'>
        <div className='border-right' style={{ width: `${!collapsed ? "260px" : "80px"}` }}>
          <Menu
            onClick={onClick}
            defaultSelectedKeys={['1']}
            defaultOpenKeys={['sub1']}
            mode="inline"
            inlineCollapsed={collapsed}
            items={items}
            className="menu-account"
          />
          <button className='btn-account' onClick={toggleCollapsed}>{collapsed ? <MenuUnfoldOutlined /> : <MenuFoldOutlined />}</button>
        </div>
        <div className='outlet'>
          <Outlet />
        </div>
      </div>
    </div>
  )
}

export default Account