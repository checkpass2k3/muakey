import "../styles/ewallet-transfer.scss"
import { InputNumber,Checkbox } from "antd"
import {useState} from "react"
import HistoryTable from "../components/HistoryTable"
import momo from "../../../../assets/images/momo.svg"

const EwalletTransfer = () => {

    const [checkNote,setCheckNote] = useState<boolean>(false)

  return (
    <div className="bank-transfer">
    <h1>Nạp tiền qua ví điện tử</h1>
    <div className="bank-transfer-content">
        <div className="bank-transfer-content-center">
            <div className="wallet-list center">
                <div className="wallet-list-item center">
                    <div>
                        <div className="center">
                        <img src={momo} alt="" />

                        </div>
                        <div className="center">Momo</div>
                        <span className="center">CK 1%</span>
                    </div>

                </div>
                
            </div>
            <div className="input-money">
                <InputNumber addonAfter="VNĐ" defaultValue={100} />
            </div>
            <div className="input-check">
                <Checkbox onChange={() => setCheckNote(!checkNote)}>Tôi hiểu rằng, khi chuyển khoản phải chính xác hoàn toàn nội dung chuyển khoản và số tiền.</Checkbox>
            </div>
            <button className={!checkNote ? "no-active":""}>Nạp tiền</button>
        </div>
    </div>
    <div className="bank-history">
        <h1>Lịch sử nạp tiền</h1>

        <div className="bank-table">
            <HistoryTable />
        </div>
    </div>
</div>
  )
}

export default EwalletTransfer