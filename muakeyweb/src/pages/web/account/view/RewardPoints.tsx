import React from 'react'
import Reward from '../components/Reward'
import "../styles/rewardpoints.scss"
import { Progress } from 'antd'
import goldRank from "../../../../assets/images/gold-rank.svg"
import {FaStarAndCrescent } from "react-icons/fa"
import {LeftOutlined,RightOutlined} from "@ant-design/icons"
import { Table } from 'antd';
import type { ColumnsType } from 'antd/es/table';

const RewardPoints = () => {

  interface DataType {
    key: React.Key;
    time: string;
    point: string;
    note: string;
  }
  
  const columns: ColumnsType<DataType> = [
    {
      title: 'Thời gian',
      dataIndex: 'time',
    },
    {
      title: 'Điểm',
      dataIndex: 'point',
    },
    {
        title: 'Note',
        dataIndex: 'note',
      },
  ];

  const data: DataType[] = [];

  return (
    <div className='reward-points'>
      <h1>Đổi điểm</h1>
      <Reward />
      <div className='flex-justify-between reward-points-content'>
        <div className='member'>
            <div className='flex-justify-between align-items-center'>
              <div className='text-name'>Rookie</div>
              <img src={goldRank} alt="" />
            </div>
            <Progress percent={30} />
            <div className='display-flex-center'><FaStarAndCrescent style={{marginRight: "5px"}} /> <span> Mua thêm 800 để thăng hạng Amateur</span></div>
        </div>
        
        <div className="history-point">
          <div className='flex-justify-between align-items-center'>
            <div className='left'>Lịch sử đổi điểm</div>
            <div className="pagination-point display-flex align-items-center">
              <LeftOutlined />
              <div className='page-point'>
                Trang 1/0
              </div>
              <RightOutlined />
            </div>
          </div>
          <div className="history-point-table">
          <Table columns={columns} dataSource={data} />
          </div>
        </div>
      </div>
    </div>
  )
}

export default RewardPoints