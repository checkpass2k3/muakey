import React, { Suspense, useEffect, useState } from 'react';
import { Routes, Route, useLocation } from 'react-router-dom';
import { Spin } from 'antd';
import routes from './routes';
import { RouteProps } from 'store/common/interface';
import Header from './layouts/Header';
import Footer from './layouts/Footer';
import Contact from './layouts/Contact';
import NavMobile from './layouts/NavMobile';
import Account from './account/view/Account';
import AccountProfile from './account/view/AccountProfile';
import ChangePassword from './account/view/ChangePassword';
import LoginHistories from './account/view/LoginHistories';
import AppConnected from './account/view/AppConnected';
import RewardPoints from './account/view/RewardPoints';
import Vouchers from './account/view/Vouchers';
import WareHouse from './account/view/WareHouse';
import RechargeHistories from './account/view/RechargeHistories';
import TransactionHistories from './account/view/TransactionHistories';
import Wishlists from './account/view/Wishlists';
import Recharge from './account/view/Recharge';
import BankTransfer from './account/view/BankTransfer';
import EwalletTransfer from './account/view/EwalletTransfer';
import CardExchange from './account/view/CardExchange';
import ProfileEdit from './account/view/ProfileEdit';

const Web = () => {
  const KEY_ROUTER = ['/web/login', '/web/register', '/web/register-success'];
  const location = useLocation();

  const [isPageLogin, setIsPageLogin] = useState<boolean>(false);

  useEffect(() => {
    if (KEY_ROUTER.includes(location.pathname)) {
      setIsPageLogin(true);
    } else {
      setIsPageLogin(false);
    }
  }, [location])

  return (
    <>
      {!isPageLogin && <Header />}
      <div className="content">
        <Routes>
          {routes.map((route: RouteProps, idx: number) => {
            return (
              route.component && (
                <Route
                  key={idx}
                  path={route.path}
                  element={
                    <Suspense fallback={<Spin />}>
                      <route.component />
                    </Suspense>
                  }
                />
              )
            );
          })}
          <Route path='/account' element={<Account />}>
            <Route path='profile' element={<AccountProfile />} />
            <Route path='profile/edit' element={<ProfileEdit />} />
            <Route path='change-password' element={<ChangePassword />} />
            <Route path='login-histories' element={<LoginHistories />} />
            <Route path='applications' element={<AppConnected />} />
            <Route path='recharge' element={<Recharge />} />
            <Route path='recharge/bank-transfer' element={<BankTransfer />} />
            <Route path='recharge/ewallet-transfer' element={<EwalletTransfer />} />
            <Route path='recharge/card-exchange' element={<CardExchange />} />
            <Route path='exchange-points' element={<RewardPoints />} />
            <Route path='vouchers' element={<Vouchers />} />
            <Route path='warehouse' element={<WareHouse />} />
            <Route path='recharge-histories' element={<RechargeHistories />} />
            <Route path='transaction-histories' element={<TransactionHistories />} />
            <Route path='wishlists' element={<Wishlists />} />
          </Route>
        </Routes>
      </div>
      {!isPageLogin && <Footer />}
      {!isPageLogin &&<Contact />}
      {!isPageLogin &&<NavMobile />}
    </>
  );
};

export default Web;
